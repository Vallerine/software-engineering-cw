<%-- 
    Document   : index
    Created on : 13-Mar-2018, 01:14:25
    Author     : nikla
--%>

<%@page import="core.*"%>
<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html><head>
    <meta charset="UTF-8">
    <title>HealthyWae - Health, Diet &amp; Fitness Tracker</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
    <link href="stylesheet.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div class="page-wrap">
    <div class="container">
        <div class="header row border-top">
            <div class="col col-12">
                <h1>HealthyWae</h1>
                <a href="/">Home</a>
            </div>            
        </div>
        <div class="row border-bottom"></div>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col col-4">
                <h2>Log In To HealthyWae</h2>
                <form>
                    E-mail
                    <input name="email" type="text"><br>
                    Password
                    <input name="password" type="text"><br>
                    <input class="button" type="submit">
                </form>
                <p>
                    <%
                        Class.forName("org.postgresql.Driver");
                        String email = request.getParameter("email");
                        String pass = request.getParameter("password");
                        if(email == null || pass == null){
                            out.println(User.getUserData(2));
                            out.println("Please enter some data...");
                        }
                        
                        %>
                </p>
            </div>
        </div>
    </div>
    <div class="push"></div>
    </div>
    
    <div class="footer-background pale-border-top">
        <div class="pale-border-bottom"></div>
        <div class="container">
            <div class="row footer">
                HealthyWae - S.A McGuinness 100139331 | E. Kemp 100128483 | J. Stefinovic 100136806 | N. Henderson 100160371
            </div>
        </div>
    </div>
    
    <script>
        var canvas = document.getElementById("graph");
        var ctx = canvas.getContext("2d");
        ctx.moveTo(0,100);
        ctx.lineTo(0,0);
        ctx.moveTo(0,100);
        ctx.lineTo(120,100);
        ctx.moveTo(0,100);
        ctx.lineTo(120,60);
        ctx.stroke();
    </script>
</body>
</html>
