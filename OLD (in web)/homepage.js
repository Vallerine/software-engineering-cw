var editing = false;
var widgetMove = false;
var transaction = false;
var widgetTiles = ["wc1", "wc2", "wc3", "wc4", "wc5", "wc6", "wc7", "wc8", "wc9"];

function main(){
    //toggleEditing();
}

function editingOff(){
	for(n=0; n<widgetTiles.length; n++){
		tile = widgetTiles[n];
		if(document.getElementById(tile).classList.contains("widgetContainer")){
			document.getElementById(tile).classList.remove("widgetContainer");
			document.getElementById(tile).classList.add("emptyWidget");
			document.getElementById(tile).innerHTML = "";
			//document.getElementById(tile).style.opacity = "0.5";
		};
	};
	editing = (!editing);
};

function editingOn(){
	for(n=0; n<widgetTiles.length; n++){
		tile = widgetTiles[n];
		if(document.getElementById(tile).classList.contains("emptyWidget")){
			document.getElementById(tile).classList.remove("emptyWidget");
			document.getElementById(tile).classList.add("widgetContainer");
			document.getElementById(tile).innerHTML = "<h4>Add a<br>widget!</h4>";
			
			document.getElementById(tile).style.opacity = "0";
		};
	};
	
	for(y=0; y<100; y++){
		for(x=0; x<widgetTiles.length; x++){
			tile = widgetTiles[x];
			var opac = parseFloat(y/100.0).toString();
			document.getElementById(tile).style.opacity = opac;
		};
		sleep(100);
		alert("hello!");
	};
	
	
	
	
	editing = true;
};





function mouseMoveEvent(event){
	if(widgetMove == true){
		document.getElementById("floatingWindow").style.left = (event.clientX-125)+"px";
		document.getElementById("floatingWindow").style.top = (event.clientY-25)+"px";
	};
};



function liftWidget(ele, event){
	var id = ele.parentNode.parentNode.id;
	if(widgetMove == false){
        editingOn();
		transaction = true;
		document.getElementById("floatingWindow").innerHTML = document.getElementById(id).innerHTML;
		document.getElementById(id).innerHTML = "<h4>Add a<br>widget!</h4>";
		document.getElementById(id).classList.remove("widget");
		document.getElementById(id).classList.add("widgetContainer");
		document.getElementById("floatingWindow").classList.remove("widgetContainer");
		document.getElementById("floatingWindow").classList.add("widget");
    
		widgetMove = true;
		setTimeout(function(){transaction = false;}, 100);
	};
	if(widgetMove == false){
		document.getElementById("floatingWindow").style.left = "0px";
		document.getElementById("floatingWindow").style.top = "-50px";
	};
};

function dropWidget(ele){
	var id = ele.id;
	if(widgetMove == true && transaction == false && document.getElementById(id).classList.contains("widgetContainer")){
		document.getElementById(id).innerHTML = document.getElementById("floatingWindow").innerHTML;
		document.getElementById(id).classList.remove("widgetContainer");
		document.getElementById(id).classList.add("widget");

		document.getElementById("floatingWindow").classList.remove("widget");
		document.getElementById("floatingWindow").classList.add("widgetContainer");
		document.getElementById("floatingWindow").innerHTML = "<h4>Add a<br>widget!</h4>";
		document.getElementById("floatingWindow").style.left = "0px";
		document.getElementById("floatingWindow").style.top = "-350px";
		widgetMove = false;
        editingOff();
	};
	
	
};

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
};



