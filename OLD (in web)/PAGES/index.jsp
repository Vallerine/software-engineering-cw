<%-- 
    Document   : index
    Created on : 13-Mar-2018, 01:14:25
    Author     : nikla
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>HealthyWae - Health, Diet &amp; Fitness Tracker</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
    <link href="stylesheet.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div class="page-wrap">
    <div class="container">
        <div class="header row border-top">
            <div class="col col-12">
                <h1>HealthyWae</h1>
                <a href="login.jsp">Log In</a>
            </div>            
        </div>
        <div class="row border-bottom"></div>
    </div>
    
    <div class="container">
        <div class="row border-top">
            <div class="col col-6">
                <h2>Activity Input</h2>
                <form>
                    First Name
                    <input type="text"><br>
                    Last Name
                    <input type="text"><br>
                    <input class="button" type="submit">
                </form>        
            </div>
            <div class="col col-6">
                <h2>Recent Analysis</h2>
                <table>
                    <tr>
                        <th>Day</td>
                        <th>Actvity</td>
                        <th>Result</td>
                    </tr>
                    <tr>
                        <td>Dit</td>
                        <td>Dah</td>
                        <td>Do</td>
                    </tr>
                    <tr>
                        <td>Doe</td>
                        <td>Ray</td>
                        <td>Me</td>
                    </tr>
                </table>   
                <canvas id="graph" class="graph"></canvas>
            </div>
        </div>
        <div class="row border-bottom">
            <div class="col col-6">
                <h2>Family Activity</h2>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
                <br><br>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
            </div>
            <div class="col col-6">
                <h2>Community Goals</h2>
            </div>
        </div>
    </div>
    <div class="push"></div>
    </div>
    
    <div class="footer-background pale-border-top">
        <div class="pale-border-bottom"></div>
        <div class="container">
            <div class="row footer">
                HealthyWae - S.A McGuinness 100139331 | E. Kemp 100128483 | J. Stefinovic 100136806 | N. Henderson 100160371
            </div>
        </div>
    </div>
    
    <script>
        var canvas = document.getElementById("graph");
        var ctx = canvas.getContext("2d");
        ctx.moveTo(0,100);
        ctx.lineTo(0,0);
        ctx.moveTo(0,100);
        ctx.lineTo(120,100);
        ctx.moveTo(0,100);
        ctx.lineTo(120,60);
        ctx.stroke();
    </script>
</body>
</html>