package validations;

import SQLController.SQLConn;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.sql.Array;
import java.sql.SQLException;
import java.util.Arrays;

/**
 *
 * @author Elliot
 */
public class Password {
    public static String getPassword(String password, byte[] salt) {
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(salt);
            //Get the hash's bytes 
            byte[] bytes = md.digest(password.getBytes());
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        } 
        catch (NoSuchAlgorithmException e) {
        }
        return generatedPassword;
    }
    
    /**
     * 
     * @param arr
     * @return 
     */
    public static byte[] getArr(Array arr) {
        String str = arr.toString();
        str = str.substring(1, str.length()-1);
        byte[] recover = new byte[16];
        int c = 0;
        for(String s : str.split(",")) {
            recover[c] = (byte)Integer.parseInt(s);
            c++;
        }
        return recover;
    }
    
    public static String sendArr(byte[] arr) {
        String str = Arrays.toString(arr);
        str = str.substring(1, str.length()-1);
        str = "{" + str + "}";
        
        return str;
    }
    
    public static Array sendObjectArr(byte[] arr) throws SQLException {
        Object[] tempArr = new Integer[16];
        if(arr.length == 16) {
            for(int i = 0; i < 16; i++)
                tempArr[i] = (int)arr[i]; 
        }
        
        try {
            return SQLConn.getConn().createArrayOf("integer",tempArr);
        }
        catch(SQLException e) {
            System.out.println("Error in Password.sendObjectArr"+e.toString());
            return null;
        }
    }
    
    public static byte[] salt() {
        try{
            SecureRandom rand = SecureRandom.getInstance("SHA1PRNG", "SUN");
            byte[] salt = new byte[16];
            rand.nextBytes(salt);
            return salt;
        }
        catch(NoSuchAlgorithmException | NoSuchProviderException e) {
            return null;
        }
    }
    
    public static boolean InternalLogin(String input, String password, byte[] salt) {
        return getPassword(input, salt).equals(password);
    }
    
    public static void main(String[] args) {
        byte[] salt = salt();
        
        String securePassword = getPassword("Wibble", salt);
        System.out.println("Wibble: " + securePassword);
         
        String regeneratedPassowrdToVerify = getPassword("Wibble", salt);
        System.out.println("Wibble: " + regeneratedPassowrdToVerify);
        
        System.out.println("Salt key: " + Arrays.toString(salt));
    }
}
