/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validations;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Niklas Henderson
 */
public class input {
    public static boolean checkString(String s){
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(s);
        return !m.find();
    }
    
    
    public static String correctString(String s){
        String toReturn = s.replaceAll("[^a-z0-9 ]", "");
        return toReturn;
    }
}
