package JavaController;

/**
 * Master model class to implement three specific methods
 * @author Niklas Henderson
 */
public abstract class Master {
    //public abstract Object clone(Object s);

    public abstract boolean submit();
    public abstract boolean delete();
    public abstract boolean update();
}
