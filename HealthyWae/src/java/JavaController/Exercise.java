 package JavaController;

import SQLController.SQLExercise;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Exercise represents an exercise that a user has completed
 * @author Elliot
 */
public class Exercise extends Master implements Comparable<Exercise> {
    
    private User user;
    private int id;
    private ExerciseType type;
    private double exerciseAmount;
    private LocalDateTime date;
    private double caloriesBurned;
    
    // <editor-fold desc="Constructors" defaultstate="collapsed">
    
    /**
     * Constructor using calculated exercise amount
     * @param user 
     * @param date
     * @param exerciseAmount
     * @param type
     */
    public Exercise(User user,
            double exerciseAmount, 
            ExerciseType type,
            LocalDateTime date) {
        this.user = user;
        this.type = type;
        this.exerciseAmount = exerciseAmount;
        this.date = date;
        this.caloriesBurned = type.getCalsPerUnit() * exerciseAmount;
        this.id = -1;
    }
    
    /**
     * Constructor with all variables
     * @param user
     * @param exerciseAmount
     * @param type
     * @param date
     * @param caloriesBurned
     */
    public Exercise(User user, 
            double exerciseAmount, 
            ExerciseType type,
            LocalDateTime date,
            double caloriesBurned) {
        this.user = user;
        this.type = type;
        this.exerciseAmount = exerciseAmount;
        this.date = date;
        this.caloriesBurned = caloriesBurned;
        this.id = -1;
    }
    
    /**
     * Default constructor
     */
    public Exercise() {
        this.user = new User();
        this.type = null;
        this.exerciseAmount = 0.0;
        this.date = LocalDateTime.MIN;
        this.caloriesBurned = 0.0;
        this.id = -1;
    }
    
    // </editor-fold>
    
    // <editor-fold desc="Getters" defaultstate="collapsed">
    /**
     * @return User of the exercise
     */
    public User getUser() { return this.user; }
    /**
     * @return the type of exercise enum
     */
    public ExerciseType getType() { return this.type; }
    /**
     * @return the amount of exercise done (depending on the enum)
     */
    public double getExerciseAmount() { return this.exerciseAmount; }
    /**
     * @return the date the exercise was completed 
     */
    public LocalDateTime getDate() { return this.date; }
    /**
     * @return the total calories burned as a result of the exercise
     */
    public double getCaloriesBurned() { return this.caloriesBurned; }
    // </editor-fold>
    
    @Override
    public boolean delete(){
        return SQLExercise.delete(this.id);
    }
    @Override
    public boolean submit(){
        return SQLExercise.newExercise(this);
    }
    public static Exercise clone(int key){
        return SQLExercise.getExercise(key);
    }
    @Override
    public boolean update(){
        return SQLExercise.update(this);
    }
    
    /**
     * @return all exercise types
     */
    public List<ExerciseType> listTypes() {
        return SQLExercise.TYPE_LIST;
    }
    
    
    @Override
    public int compareTo(Exercise e) {
        return this.getDate().compareTo(e.getDate());
    }
    
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("Exercise: ");
        str.append("\nExercise Type: ").append(this.getType().getType());
        str.append("\nAmount completed: ").append(this.exerciseAmount);
        str.append(" ").append(this.type.getUnit());
        str.append("\nDate and Time completed: ").append(this.date.toString());
        return str.toString();
    }
}
