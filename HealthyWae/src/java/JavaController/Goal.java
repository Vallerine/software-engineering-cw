package JavaController;

import SQLController.SQLGoal;
import java.time.LocalDate;

/**
 * Goal represents a specific target weight for a user for a specific date
 * @author Elliot
 */
public abstract class Goal implements Comparable<Goal> {
    protected final User user;
    protected final String goalName;
    protected final double targetFloat;
    protected final LocalDate targetDate;
    protected final LocalDate date_set;
    protected boolean completed;
    
    // <editor-fold desc="Constructors" defaultstate="collapsed">
    /**
     * Constructor with all variables provided
     * @param user
     * @param goalName
     * @param targetFloat
     * @param targetDate
     * @param completed 
     */
    public Goal(User user, String goalName, double targetFloat, 
            LocalDate targetDate, LocalDate dateSet, boolean completed) {
        this.user = user;
        this.goalName = goalName;
        this.targetFloat = targetFloat;
        this.targetDate = targetDate;
        this.completed = completed;
        this.date_set = dateSet;
    }
    /**
     * Default constructor
     */
    public Goal() {
        this.user = new User();
        this.goalName = "";
        this.targetFloat = 0.0;
        this.targetDate = LocalDate.MIN;
        this.date_set = LocalDate.MIN;
        this.completed = false;
    }
    
    // </editor-fold>
    
    // <editor-fold desc="Getters" defaultstate="collapsed">
    
    /**
     * @return goal name
     */
    public String getGoalName() { return this.goalName; }
    
    /**
     * @return target weight
     */
    public double getTarget() { return this.targetFloat; }
    
    public LocalDate getSetDate() { return this.date_set; }
    /**
     * @return target date
     */
    public LocalDate getTargetDate() { return this.targetDate; }
    
    /**
     * @return completed status
     */
    public boolean getCompleted() { return this.completed; }
    
    public boolean complete(){
        this.completed = true;
        
        return SQLGoal.completeGoal((ExerciseGoal) this);
        
        
        
    }
    // </editor-fold>
    
    public abstract boolean submit();
    public abstract double getProgress();
    public abstract boolean delete(String o, User u);
    public abstract ExerciseType getType();
    
    @Override
    public int compareTo(Goal g) {
        return this.getTargetDate().compareTo(g.getTargetDate());
    }
}