package JavaController;

import SQLController.SQLGoal;
import java.time.LocalDate;


/**
 * Exercise goal class to inherit from goal and hold the goal type
 * @author Elliot
 */
public class ExerciseGoal extends Goal{
    private final ExerciseType type;
    private int goalID;
    
    // <editor-fold desc="Constructors" defaultstate="collapsed">
    /**
     * Full constructor
     * @param user
     * @param goalName
     * @param targetFloat
     * @param targetDate
     * @param completed
     * @param dateSet
     * @param type 
     */
    public ExerciseGoal(int id, User user, 
            String goalName, 
            double targetFloat, 
            LocalDate targetDate,
            LocalDate dateSet,
            boolean completed,
            ExerciseType type) {
        super(user, goalName, targetFloat, targetDate, dateSet, completed);
        this.type = type;
        this.goalID = id;
    }
    
    public ExerciseGoal(User user, 
            String goalName, 
            double targetFloat, 
            LocalDate targetDate,
            LocalDate dateSet,
            boolean completed,
            ExerciseType type) {
        super(user, goalName, targetFloat, targetDate, dateSet, completed);
        this.type = type;
        this.goalID = -1;
    }
    
    
    /**
     * Default constructor
     */
    public ExerciseGoal() {
        super(new User(),
                "",
                0.0,
                LocalDate.MIN,
                LocalDate.MIN,
                false);
        this.type = null;
    }
    // </editor-fold>
    
    /**
     * @return the type of exercise 
     */
    @Override
    public ExerciseType getType() {
        return this.type;
    }
    
    @Override
    public double getProgress(){
        return 0.0;
    }
    @Override
    public boolean delete(String s, User u){
        return SQLGoal.deleteGoal(s, u);
    }
    
    @Override
    public boolean submit(){
        return SQLGoal.newGoal(this, this.user);
    }

    public int getID(){
        return this.goalID;
    }
    
    
}
