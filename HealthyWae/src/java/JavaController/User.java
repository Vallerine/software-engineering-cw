package JavaController;

import SQLController.SQLConn;
import java.util.List;
import validations.EmailValidator;
import validations.Password;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import SQLController.SQLExercise;
import SQLController.SQLGoal;
import SQLController.SQLMeal;
import SQLController.SQLUser;
import java.time.DayOfWeek;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * User represents a specific user that intends to use the
 * application
 * @author Elliot
 */
public class User extends Master{    
    private String password;
    private byte[] salt;
    private final int userID;
    private String email;
    private String forename;
    private String surname;
    private double height; //default in centimetres
    private double weight; //default in kilograms
    private double bodyMassIndex;
    private UserGroup group;
    private final ArrayList<Goal> goalsList;
    private final List<Exercise> exerciseList;
    private List<Meal> mealList;
    
    // <editor-fold desc="Constructors" defaultstate="collapsed">
    /**
     * Constructor for login
     * @param userID
     * @param email
     * @param password
     * @param salt
     * @param forename
     * @param surname
     * @param height
     * @param weight
     */
    public User(
            int userID,
            String email, 
            String password,
            byte[] salt,
            String forename,
            String surname,
            double height,
            double weight) {
        this.userID = userID;
        this.email = email;
        this.password = password;
        this.salt = salt;
        this.forename = forename;
        this.surname = surname;
        this.height = height;
        this.weight = weight;
        this.goalsList = new ArrayList<>();
        this.exerciseList = new ArrayList<>();
        this.mealList = new ArrayList<>();
        this.bodyMassIndex = bodyMassIndex();
        this.group = new UserGroup();
    }
    
    /**
     * Constructor for a new or newly-retrieved user (with a new goalsList and exerciseList)
     * @param userID
     * @param email
     * @param password
     * @param forename
     * @param surname
     * @param height
     * @param weight
     */
    public User(
            int userID,
            String email, 
            String password,
            String forename,
            String surname,
            double height,
            double weight) {
        this.userID = userID;
        this.email = email;
        this.forename = forename;
        this.surname = surname;
        this.height = height;
        this.weight = weight;
        this.bodyMassIndex = bodyMassIndex();
        this.goalsList = new ArrayList<>();
        this.exerciseList = new ArrayList<>();
        this.mealList = new ArrayList<>();
        
        this.salt = Password.salt();
        this.password = Password.getPassword(password, this.salt);
    }
    
    /**
     * Constructor for everything BUT password and salt
     * only for use when the software needs user data but not to display 
     * anything sensitive
     * @param userID
     * @param email
     * @param forename
     * @param surname
     * @param height
     * @param weight 
     */
    public User(int userID, 
            String email, 
            String forename, 
            String surname, 
            double height, 
            double weight) {
        this.userID = userID;
        this.email = email;
        this.forename = forename;
        this.surname = surname;
        this.height = height;
        this.weight = weight;
        this.bodyMassIndex = bodyMassIndex();
        this.goalsList = SQLGoal.getGoals(this);
        this.exerciseList = SQLExercise.getExercises(this);
        this.mealList = SQLMeal.getMeals(this);
        this.salt = null;
        this.password = null;
    }
    
    /**
     * Default constructor
     */
    public User(){
        this.userID = -1;
        this.email = "null";
        this.forename = "null";
        this.surname = "null";
        this.height = 0.0;
        this.weight = 0.0;
        this.bodyMassIndex = 0.0;
        this.goalsList = new ArrayList<>();
        this.exerciseList = new ArrayList<>();
        this.mealList = new ArrayList<>();
        this.salt = null;
        this.password = "null";
    }
    
    //</editor-fold>

    @Override
    public boolean submit(){
        System.out.println("Submit function not supported by User");
        return false;
    }
    @Override
    public boolean update(){
        return SQLUser.update(this);
    }
    @Override
    public boolean delete(){
        return SQLUser.delete(this);
    }

    /**
     * Clones the user
     * @param email
     * @return the user object
     */
    public static User clone(String email){
        return SQLUser.getUser(email);
    }

    /**
     * Registers a new user to the system
     * @param forename
     * @param surname
     * @param password
     * @param email
     * @return whether the registration was successful
     */
    public static boolean register(
            String forename,
            String surname,
            String password,
            String email){
        return SQLUser.newUser(email, password, forename, surname, 0.0, 0.0);
    }
    
    /**
     * Gets the user from a given session
     * @param ip
     * @param id
     * @return user
     */
    public static User getUserFromSession(String ip, String id){
        try{            
            Statement stm = SQLConn.getConn().createStatement();
            ResultSet result = stm.executeQuery(String.format(SQLConn.GET_SESSION_DATA_QUERY, ip, id));           
            result.next();
            int member_id = result.getInt("member_id");
            User user = SQLUser.getUser(member_id);
            
            return user;
        }catch(SQLException e){
            System.out.println("User getUserFromSession: "+e.toString());
            return null;
        }
    }
    public void addGoals(ArrayList<Goal> goals){
        this.goalsList.addAll(goals);
    } 
    
    public boolean validatePassword(String password){
        return (password.equals(Password.getPassword(password, this.salt))); 
    }
    
    /**
     * Logs in a user
     * @param email
     * @param password
     * @param ip
     * @return a new session key if the user logs in successfully
     */
    public static String login(String email, String password, String ip) {
        User user = SQLUser.login(email, password);
        if(user != null){
            Session.removeUserSessions(user);
            String key = Session.createSession(user, ip);
            return key;
        }else{
            System.out.println("Error in User.login");
            return null;
        }
    }
    
    
    /**
     * @param email
     * @return whether the email was valid (and set as a result)
     */
    public boolean setEmail(String email) {
        EmailValidator validate = new EmailValidator();
        if(validate.validateEmail(email)) {
            this.email = email;
            SQLUser.update(this, 6, email);
            return true;
        }
        else return false;
    }
    
    /**
     * Sets the name (validate later)
     * @param forename
     * @param surname 
     */
    public void setName(String forename, String surname) {
        this.forename = forename;
        this.surname = surname;
        SQLUser.update(this, 3, this.forename);
        SQLUser.update(this, 4, this.surname);
    } 
    /**
     * Sets the forename
     * calls setName
     * @param forename 
     */
    public void setForename(String forename) {
        setName(forename, this.surname);
    }
    /**
     * Sets the surname
     * calls setName
     * @param surname 
     */
    public void setSurname(String surname) {
        setName(this.forename, surname);
    }
    
    /**
     * Setting height in metric units (centimetres)
     * @param height 
     */
    public void setHeight(double height) {
        this.height = height;
        bodyMassIndex();
    }
    
    /**
     * Setting height in imperial units (feet and inches)
     * @param feet
     * @param inches 
     */
    public void setHeight(int feet, int inches) {
        this.height = ((double)feet + ((double)inches/12)) * 30.48;
        bodyMassIndex();
    }
    
    /**
     * Setting weight in metric units (kilograms)
     * @param weight 
     */
    public void setWeight(double weight) {
        this.weight = weight;
        bodyMassIndex();
    }
    
    /**
     * Setting weight in imperial units (stone and pounds)
     * @param stone
     * @param pounds 
     */
    public void setWeight(int stone, double pounds) {
        this.weight = ((double)stone + (pounds/14)) * 6.35029;
        bodyMassIndex();
    }
    
    /**
     * Sets the user group
     * @param u 
     */
    public void setGroup(UserGroup u) { 
        this.group = u; 
    }
    
    // <editor-fold desc="Getters and Setters" defaultstate="collapsed">
    
    /**
     * Kicks a user from their group
     */
    public void kickFromGroup() {
        this.group = null;
    }
    
    /**
     * Resets the user's password to a new password
     * @param email
     * @param newPassword 
     */
    private boolean resetPassword(String oldPassword, String newPassword) {
        if(Password.InternalLogin(oldPassword, this.password, this.salt)) {
            byte[] newSalt = Password.salt();
            String newPassKey = Password.getPassword(newPassword, newSalt);
            
            this.password = newPassKey;
            this.salt = newSalt;
            return SQLUser.resetPassword(this.userID, this.password, this.salt);
        }
        return false;
    }
    
    /**
     * Set and return bodyMassIndex
     * @return bodyMassIndex
     */
    public final double bodyMassIndex() {
        return this.bodyMassIndex = this.weight / Math.pow(this.height/100, 2);
    }
    
    /**
     * @return user ID 
     */
    public int getUserID() { return this.userID; }
    
    /**
     * @return email address
     */
    public String getEmail() { return this.email; }
    
    /**
     * @return forename 
     */
    public String getForename() { return this.forename; }
    
    /**
     * @return surname 
     */
    public String getSurname() { return this.surname; }
    
    /**
     * @return user group 
     */
    public UserGroup getGroup() { return this.group; }
    
    /**
     * @return both names 
     */
    public String[] getName() {
        String[] str = {this.forename, this.surname};
        return str;
    }
    
    /**
     * @return both names (formatted nicely)
     */
    public String getNameFormat() {
        return getName()[1].toUpperCase() + ", " + getName()[0];
    }
    
    /**
     * @return height
     */
    public double getHeight() { return this.height; }
    
    /**
     * @return weight 
     */
    public double getWeight() { 
        ArrayList<Exercise> exercises = SQLExercise.getExercises(this);
        ArrayList<Exercise> weights = new ArrayList<>();
        
        for(Exercise e : exercises){
            if(!e.getType().getType().equals("weight")){ continue; }
            weights.add(e);
        }
        if(weights.size() > 0){
            Collections.sort(weights);
            return weights.get(0).getExerciseAmount();
        }
        else{
            return 0.0;
        }
        
    }
    
    /**
     * @return list of goals 
     */
    public List<Goal> getGoals() {
        return this.goalsList; }
    
    /**
     * @return list of exercises
     */
    public List<Exercise> getExercises() { return this.exerciseList; }
    
    /**
     * @return list of meals
     */
    public List<Meal> getMeals() { return this.mealList; }
    
    /**
     * @return salt key
     */
    public byte[] getSalt() { return this.salt; }
    
    /**
     * @return password key
     */
    public String getPassword() { return this.password; }
    // </editor-fold>
    
    /**
     * Adds an exercise to the list of exercises
     * @param e exercise
     */
    public void addExercise(Exercise e) {
        this.exerciseList.add(e);
        Collections.sort(this.exerciseList);
    }
    public void addExercises(List<Exercise> e){
        this.exerciseList.addAll(e);
    }
    
    /**
     * Adds a goal to the list of goals
     * @param g goal
     */
    public void addGoal(Goal g) {
        this.goalsList.add(g);
        Collections.sort(this.goalsList);
    }
    
    /**
     * Adds a user to a user group
     * @param g 
     */
    public void addGroup(UserGroup g){
        this.group = g;
    }
    
    /**
     * Add a group of meals to the user
     * @param meals 
     */
    public void addMeals(List<Meal> meals){
        this.mealList = meals;
    }
    
    /**
     * Gets the total score from a week to use in the group leader board 
     * @return the score from the past 7 days
     */
    public double getScore(){
        double score = 0.0;
        for(Exercise e : this.exerciseList){
            int daysLeft = (int) ChronoUnit.DAYS.between(LocalDate.now(), e.getDate());
            if(daysLeft > 7){ continue; }
            else{
                score+= e.getCaloriesBurned();
            }
        }
        return score;
    }
    
    public double calculateScore() {
        int index = 0;
        List<Exercise> weekList = null;
        
        //If the last exercise done was before the past monday
        if(this.exerciseList.get(0).getDate().isBefore(LocalDateTime.now().with(DayOfWeek.MONDAY)))
            return 0;
        
        //Filter the exercises from the past week
        for(Exercise e : this.exerciseList) {
            if(e.getDate().isBefore(LocalDateTime.now().with(DayOfWeek.MONDAY))) {
                weekList = this.exerciseList.subList(0, index);
                break;
            }
            index++;
        }
        
        //If there aren't any 
        if(weekList == null) return 0;
        
        //Totalling calories burned over the week
        double totalCals = 0;
        for(Exercise e : weekList) {
            totalCals += e.getCaloriesBurned();
        }
        
        return totalCals;
    }
    
    /**
     * toString with password key
     * @return 
     */
    public String toStringPass() {
        StringBuilder str = new StringBuilder(this.toString());
        str.append(String.format("%8s: %s\n", "Pass", this.password));
        str.append(String.format("%8s: %s\n", "Salt", Arrays.toString(this.salt)));
        
        return str.toString();
        
    }
    
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(this.getNameFormat()).append(":\n");
        str.append(String.format("%8s: %s\n", "Email", this.getEmail()));
        str.append(String.format("%8s: %.2f CM\n", "Height", this.getHeight()));
        str.append(String.format("%8s: %.2f Kg\n", "Weight", this.getWeight()));
        str.append(String.format("%8s: %.2f\n", "BMI", this.bodyMassIndex()));
        str.append(String.format("%8s: %d\n", "# Goals", this.goalsList.size()));
        
        return str.toString();
    }
}
