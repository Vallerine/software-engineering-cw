package JavaController;

import SQLController.SQLConn;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;

/**
 * A class to model the user's session - not the user itself
 * @author Niklas
 */
public class Session {
    
    // <editor-fold desc="Constructors" defaultstate="collapsed">
    /**
     * Creates a new session
     * @param user
     * @param ip
     * @return the session key
     */
    public static String createSession(User user, String ip){
        try{
            LocalDateTime date = LocalDateTime.now();
            MessageDigest md = MessageDigest.getInstance("MD5");
            String key = user.getUserID()+ip;
            byte[] bytes = md.digest(key.getBytes());
            
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));}
            key = sb.toString();
            
            Statement stm = SQLConn.getConn().createStatement();
            return stm.executeUpdate(String.format(SQLConn.NEW_SESSION_STATEMENT, 
                    ip,
                    user.getUserID(),
                    java.sql.Timestamp.valueOf(date),
                    key)) == 1 ? key : null;
        }catch(SQLException | NoSuchAlgorithmException e){
            System.out.println("ERROR: "+e.toString());
            return null;
        }
    }
    
    /**
     * Removes all possible sessions of the user
     * @param user
     * @return whether all user sessions have been removed successfully
     */
    public static boolean removeUserSessions(User user){
        try{
            Statement stm = SQLConn.getConn().createStatement();
            stm.executeUpdate(String.format(SQLConn.REMOVE_OLD_SESSIONS_STATEMENT, user.getUserID()));
            return true;
        }catch(SQLException e){
            System.out.println("ERROR: "+e.toString());
            return false;
        }
    }
    
    /**
     * Checks whether a session is valid or not
     * @param ip
     * @param id
     * @return whether the session is valid
     */
    public static boolean checkSession(String ip, String id){
        try{            
            Statement stm = SQLConn.getConn().createStatement();
            ResultSet result = stm.executeQuery(String.format(SQLConn.GET_SESSION_DATA_QUERY, ip, id));
            
            result.next();
            LocalDateTime lastseen = result.getTimestamp("lastseen").toLocalDateTime();
            String sessionCode = result.getString("id");
            
            LocalDateTime now = LocalDateTime.now();
            return !((now.getMinute() > lastseen.getMinute() + 15) || (id.compareTo(sessionCode) != 0));
        }catch(SQLException e){
            System.out.println(e.toString());
            return false;
        }
    }
    
    /**
     * Updates the session's last seen timer
     * @param ip
     * @param id
     * @return whether the timer updated successfully
     */
    public static boolean updateSession(String ip, String id){
        try{            
            Statement stm = SQLConn.getConn().createStatement();
            LocalDateTime now = LocalDateTime.now();
            stm.executeUpdate(String.format(SQLConn.UPDATE_SESSION_STATEMENT, java.sql.Timestamp.valueOf(now), ip, id));
            return true;
        }catch(SQLException e){
            System.out.println(e.toString());
            return false;
        }
    }
    
    /**
     * Logs the session out
     * @param ip
     * @param id
     * @return if the logout was successful
     */
    public static boolean logout(String ip, String id){
        try{            
            Statement stm = SQLConn.getConn().createStatement();
            stm.executeUpdate(String.format(SQLConn.LOGOUT_STATEMENT, id));
            return true;
        }catch(SQLException e){
            System.out.println(e.toString());
            return false;
        }
    }
   
}
