package JavaController;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.*;
import SQLController.SQLConn;
import SQLController.SQLExercise;
import SQLController.SQLMeal;
import SQLController.SQLUser;
import java.time.LocalDateTime;

/**
 * PRIMARY CLASS FOR TESTING PURPOSES
 * @author Elliot
 */
public class HealthyWae extends SQLConn {
    
    
    public static void main(String[] args) throws 
            ClassNotFoundException, 
            SQLException, 
            NoSuchAlgorithmException, 
            NoSuchProviderException {
        
        // <editor-fold desc="Testing" defaultstate="collapsed">
        
        
        //SQLExercise.newExerciseType("Running", "minutes", 11.4);        
        String key = User.login("test@wibble.com", "test1", "0.0.0.0");
        
        // </editor-fold>
        
        close();
    }
}
