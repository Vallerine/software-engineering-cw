package JavaController;

import SQLController.SQLGroup;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * UserGroup represents a family/friendship group that
 * intends to use the application
 * @author Elliot
 */
public class UserGroup extends Master {
    private String groupName;
    private String desc;
    private final ArrayList<User> userList;
    private User admin;
    private String password;
    
    /**
     * Comparator class to compare user scores
     */
    private class CompareScore implements Comparator<User> {
        @Override
        public int compare(User u1, User u2) {
            //Casted to int because getScore is an 
            return (int)(u1.getScore() - u2.getScore());
        }
    }
    
    
    @Override
    public boolean delete(){
        return SQLGroup.deleteGroup(this.groupName);
    }
    @Override
    public boolean submit(){
        return SQLGroup.newGroup(this);
    }
    public static UserGroup clone(String groupname){
        UserGroup g = SQLGroup.getGroup(groupname);
        return g;
    }
    @Override
    public boolean update(){
        return SQLGroup.updateGroup(this);
    }
    
    public boolean add(List<User> users){
        this.userList.addAll(users);
        return true;
    }
    
    /**
     * Authenticate the password from a user group
     * (for the user to join said group)
     * @param name
     * @param password
     * @return whether the authentication passed
     */
    public static boolean authenticate(String name, String password){
        return SQLGroup.authenticate(name, password);
    }
    
    
    
    // <editor-fold desc="Constructors" defaultstate="collapsed">
    
    /**
     * Constructor with an empty user list
     * @param name
     * @param password
     * @param desc
     * @param u 
     */
    public UserGroup (String name, String password, String desc, User u){
        this.groupName = name;
        this.desc = desc;
        this.userList = new ArrayList<>();
        this.admin = u;
        this.password = password;
    }
    /**
     * Default constructor
     */
    public UserGroup(){
        this.admin = new User();
        this.desc = "";
        this.groupName = "";
        this.userList = new ArrayList<>();
        this.password = "";
    }
    


    // </editor-fold>
    
    // <editor-fold desc="Getters and Setters" defaultstate="collapsed">
    
    /**
     * @return group name
     */
    public String getGroupName() { return this.groupName; }
    /**
     * @return all users
     */
    public List<User> getUsers() { return this.userList; }
    /**
     * @return the password of the group (encrypted) 
     */
    public String getPassword() { return this.password; }
    /**
     * @return number of users in the group
     */
    public int numOfUsers() { return this.userList.size(); }
    /**
     * @return the description of the group
     */
    public String getDesc(){ return this.desc; }
    /**
     * @return group administrator
     */
    public User getAdmin() { return this.admin; }
    
    /**
     * Change the name of the group
     * @param str 
     */
    public void setGroupName(String str) { this.groupName = str; }
    /**
     * Change the group administrator
     * @param adminIndex 
     */
    public void setGroupAdmin(int adminIndex) {
        this.admin = this.userList.get(adminIndex);
    }
    
    // </editor-fold>
    
    /**
     * Adds a single user to the user list
     * @param user 
     */
    public void add(User user) {
        if(!hasUser(user)) {
            user.addGroup(this);
            this.userList.add(user); 
        }
    }
    
    /**
     * Gets an array of users in order of score
     * for use in displaying a leader board
     * @return 
     */
    public User[] getUserScores() {
        List<User> tmpList = this.userList;
        Collections.sort(tmpList, new CompareScore());
        
        return tmpList.toArray(new User[this.userList.size()]);
    }
    
    /**
     * Checks whether there is a specific user in the group
     * @param user
     * @return whether the user is there
     */
    public boolean hasUser(User user) {
        return this.userList.contains(user);
    }
    
    /**
     * Kicks a user from a group
     * @param user
     * @return whether the kick was successful
     */
    public boolean kickUser(User user) {
        if (SQLGroup.kickUser(user, this)) {
            user.kickFromGroup();
            this.userList.remove(user);
            return true;
        }
        return false;
    }
    
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("Group: ").append(this.groupName).append('\n');
        this.userList.forEach(user ->
            str.append(user.toString()));
        
        return str.toString();
    }
    
}