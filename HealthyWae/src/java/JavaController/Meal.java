package JavaController;

import SQLController.SQLMeal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Meal class to manage each meal the user consumes
 * @author Elliot
 */
public class Meal extends Master{
    
    /**
     * Nested class to handle the quantity of food types
     * (i.e snack amounts)
     */
    class FoodTypeQuantity {
        public final FoodType type;
        public final int quantity;
        
        
        /**
         * Basic constructor with the type of food and the quantity of food
         * @param type
         * @param quantity 
         */
        public FoodTypeQuantity(FoodType type, int quantity) {
            this.type = type;
            this.quantity = quantity;
        }
    }
    
    // In SQL, this is defined as a separate table to hold User IDs
    // and the food ID, along with quantity
    private final List<FoodTypeQuantity> foodQuantityList;
    
    private final double calories;
    private final User user;
    private final LocalDateTime date;
    private int mealID;

    // <editor-fold desc="Constructors" defaultstate="collapsed">
    /**
     * Constructor with arrays of all data
     * @param user
     * @param foodType
     * @param quantity
     * @param calories
     */
    public Meal(User user, FoodType foodType[], int quantity[], double calories) {
        this.foodQuantityList = new ArrayList<>();

        if(foodType.length == quantity.length)
            for(int i = 0; i < foodType.length; i++) {
                this.foodQuantityList.add(new FoodTypeQuantity(foodType[i], quantity[i]));
            }
        
        this.calories = calories;
        this.user = user;
        this.date = LocalDateTime.now();
    }
    /**
     * Constructor to use when loading in from the database
     * @param user
     * @param foodType
     * @param quantity
     * @param calories
     * @param date 
     * @param id 
     */
    public Meal(User user, FoodType foodType[], int quantity[], double calories,
            LocalDateTime date, int id) {
        this.foodQuantityList = new ArrayList<>();

        if(foodType.length == quantity.length)
            for(int i = 0; i < foodType.length; i++) {
                this.foodQuantityList.add(new FoodTypeQuantity(foodType[i], quantity[i]));
            }
        
        this.calories = calories;
        this.user = user;
        this.date = date;
        this.mealID = id;
    }
    /**
     * Empty constructor with just user
     * @param user 
     */
    public Meal(User user) {
        this.user = user;
        this.foodQuantityList = new ArrayList<>();
        this.calories = 0;
        this.date = LocalDateTime.now();
    }
    /**
     * Default Constructor
     */
    public Meal() {
        this.user = new User();
        this.foodQuantityList = new ArrayList<>();
        this.calories = 0;
        this.date = LocalDateTime.MIN;
    }
    // </editor-fold>

    // <editor-fold desc="Getters and Setters" defaultstate="collapsed">
    /**
     * @return the list of food in the meal
     */
    public FoodType[] getFoodList() { 
        FoodType[] list = new FoodType[this.foodQuantityList.size()];
        for(int i = 0; i < this.foodQuantityList.size(); i++)
            list[i] = this.foodQuantityList.get(i).type;
        return list;
    }
    /**
     * @return the quantities of food in each corresponding 
     * position of getFoodList
     */
    public int[] getQuantities() {
        int[] list = new int[this.foodQuantityList.size()];
        for(int i = 0; i < this.foodQuantityList.size(); i++)
            list[i] = this.foodQuantityList.get(i).quantity;
        return list;    
    }
    /**
     * @return calorie count in the whole meal
     */
    public double getCalories() { return this.calories; }
    /**
     * @return the user of the meal
     */
    public User getUser() { return this.user; }
    /**
     * @return the date this meal was consumed
     */
    public LocalDateTime getDate() { return this.date; }
    /**
     * @return the number of food types
     */
    public int getNumType() { return this.foodQuantityList.size(); }
    /**
     * @return the meal id
     */
    public int getID(){ return this.mealID; }
    // </editor-fold>
    
    
    @Override
    public boolean update(){
        return false;
    }
    @Override
    public boolean delete(){
        return false;
    }
    @Override
    public boolean submit(){
        return SQLMeal.newMeal(this);
    }
    

    /**
     * Add one of a type of food
     * @param type 
     */
    public void addFood(FoodType type) {
        addFood(type, 1);
    }
    
    /**
     * Add a specified number of type of food
     * @param type
     * @param quantity 
     */
    public void addFood(FoodType type, int quantity) {
        for(int i = 0; i < this.foodQuantityList.size(); i++) {
            if(this.foodQuantityList.get(i).type.equals(type)) {
                this.foodQuantityList.set(i, new FoodTypeQuantity(
                    this.foodQuantityList.get(i).type,
                    this.foodQuantityList.get(i).quantity+quantity));
                return;
            }
        }
        this.foodQuantityList.add(new FoodTypeQuantity(type, quantity));
    }
    
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("\nDate: ").append(this.date.toString());
        str.append("\nUser: ").append(this.user.getNameFormat());
        str.append("\nFood:");
        for(FoodTypeQuantity food : foodQuantityList) {
            str.append("\n  ").append(food.type.toString());
            str.append(", ").append(food.quantity);
        }
        return str.toString();
    }
}
