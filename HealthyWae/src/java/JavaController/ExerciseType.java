package JavaController;

import SQLController.SQLExerciseType;


/**
 * Type class to handle the different types of exercise
 * @author Elliot
 */
public class ExerciseType implements Comparable<ExerciseType> {
    private int index;
    private String type;
    private String unit;
    private double calsPerUnit;

    /**
     * Full constructor to handle types of exercises
     * @param index
     * @param type
     * @param unit
     * @param calsPerUnit 
     */
    public ExerciseType(int index, String type, String unit, double calsPerUnit) {
        this.type = type;
        this.unit = unit;
        this.calsPerUnit = calsPerUnit;
        this.index = index;
    }
    /**
     * Constructor without an index
     * @param type
     * @param unit
     * @param calsPerUnit 
     */
    public ExerciseType(String type, String unit, double calsPerUnit) {
        this.type = type;
        this.unit = unit;
        this.calsPerUnit = calsPerUnit;
        this.index = -1;
    }

    /**
     * Default constructor
     */
    public ExerciseType() {
        this.type = null;
        this.unit = "";
        this.calsPerUnit = 0.0;
        this.index = -1;
    }


    // <editor-fold desc="Getters and Setters" defaultstate="collapsed">
    /**
     * @return 
     */
    public String getType() { return this.type; }
    public String getUnit() { return this.unit; }
    public int getIndex() {return this.index; }
    public double getCalsPerUnit() { return this.calsPerUnit; }

    /**
     * Set calories per unit
     * @param calsPerUnit 
     */
    public void setCalsPerUnit(int calsPerUnit) {
        this.calsPerUnit = calsPerUnit;
    }
    
    // </editor-fold>
    
    /**
     * Clones the type of exercise
     * @param name
     * @return exercise type object
     */
    public static ExerciseType clone(String name){
        return SQLExerciseType.clone(name);
    }
    
    @Override
    public int compareTo(ExerciseType e) {
        return this.type.compareTo(e.type);
    }

    @Override
    public String toString() {
        String output = new String();
        output += this.type + "\n";
        output += this.unit + "\n";
        output += Double.toString(this.calsPerUnit);
        return output;
    }
}
