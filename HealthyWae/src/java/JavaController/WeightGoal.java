package JavaController;

import SQLController.SQLExerciseType;
import java.time.LocalDate;

/**
 *
 * @author Elliot
 */
public class WeightGoal extends Goal {

    // <editor-fold desc="Constructors" defaultstate="collapsed">
    /**
     * Constructor that inherits from Goal
     * @param user
     * @param goalName
     * @param targetFloat
     * @param targetDate
     * @param dateSet
     * @param completed 
     */
    public WeightGoal(User user, 
            String goalName, 
            double targetFloat, 
            LocalDate targetDate,
            LocalDate dateSet,
            boolean completed) {
        super(user, goalName, targetFloat, targetDate, dateSet, completed);
    }
    /**
     * Default constructor
     */
    public WeightGoal() {
        super(new User(), "", 0.0, LocalDate.MIN, LocalDate.MIN, false);
    }
    // </editor-fold>    
    
    @Override
    public boolean getCompleted() { 
        if(this.completed)
            return true;
        else {
            LocalDate date = LocalDate.now();
            if(date.isBefore(this.targetDate) && user.getWeight() <= this.targetFloat)
                return this.completed = true;
            else
                return this.completed = false;
        }
    }
    
    /**
     * @return the type of goal (Weight) 
     */
    @Override
    public ExerciseType getType() {
        return SQLExerciseType.clone("weight");
    }
    
    
    @Override
    public boolean delete(String s, User u){
        return false;
    }
    
    @Override
    public double getProgress(){
        return 0.0;
    }
    @Override
    public boolean submit(){return false;}

    

}
