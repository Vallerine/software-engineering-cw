package JavaController;

/**
 * Food type class to handle the different types of food in a meal
 * and their calorific count per portion
 * @author Elliot
 */
public class FoodType extends Master implements Comparable<FoodType>{
    private int typeID;
    private String type;
    private double calsPerPortion;

    /**
     * Full constructor to store types of food (and drink!)
     * @param typeID
     * @param type
     * @param calsPerPortion 
     */
    public FoodType(int typeID, String type, double calsPerPortion) {
        this.typeID = typeID;
        this.type = type;
        this.calsPerPortion = calsPerPortion;
    }
    /**
     * Default constructor
     */
    public FoodType() {
        this.typeID = -1;
        this.type = "";
        this.calsPerPortion = 0.0;
    }

    // <editor-fold desc="Getters and Setters" defaultstate="collapsed">
    /**
     * @return the ID of the type of food
     */
    public int getTypeID() { return this.typeID; }
    /**
     * @return the name of the type of food
     */
    public String getType() { return this.type; }
    /**
     * @return the (average) amount of calories per portion of this food type
     */
    public double getCalsPerPortion() { return this.calsPerPortion; }

    /**
     * Sets the number of calories per portion if it needed changing
     * @param calsPerPortion new value to use
     */
    public void setCalsPerPortion(int calsPerPortion) {
        this.calsPerPortion = calsPerPortion;
    }
    // </editor-fold>

    @Override
    public int compareTo(FoodType e) {
        return this.type.compareTo(e.type);
    }

    @Override
    public String toString() {
        return this.type;
    }
    
    
    
    //Some of these are redundant
    @Override
    public boolean delete(){ return true; };
    @Override
    public boolean submit(){ return true; };

    /**
     * Clones the type of food
     * @param a
     * @return the food type object
     */
    public static FoodType clone(String a){ 
        return null; 
    };

    @Override
    public boolean update(){ return false; }
}