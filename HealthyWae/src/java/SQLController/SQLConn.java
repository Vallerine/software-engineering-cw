package SQLController;

import java.sql.*;


/**
 * This class handles all SQL connections and should be launched when the
 * software is started
 * @author Elliot
 */
public class SQLConn {
    static final String JDBC_DRIVER = "org.postgresql.Driver"; 
    private static final String SQL_IP = "jdbc:postgresql://139.59.183.94:5432/softengi";
    private static final String SQL_USER = "softengi";
    private static final String SQL_PASS = "FloopyFlop!";
    
    private static Connection CONN = null;
    
    // <editor-fold desc="SQL Statements" defaultstate="collapsed">
    
    /**
     * Get the member with the matching email
     */
    public static final String GET_MEMBER_QUERY = 
            "SELECT * FROM member WHERE email = '%s';";
    
    /**
     * Get the member information without any password or salt information
     */
    public static final String GET_SAFE_MEMBER_QUERY =
            "SELECT * FROM member_safe WHERE %s = %d;";
    
    /**
     * Get all of something from something else
     */
    public static final String GET_ALL_QUERY = 
            "SELECT * FROM %s WHERE %s = %d;";
    /**
     * Get all of something from something else
     * WITH A STRING
     */
    public static final String GET_ALL_QUERY2 = 
            "SELECT * FROM %s WHERE %s = '%s';";    
    
    /**
     * Get all of something without a filter (the whole table)
     */
    public static final String GET_ALL_QUERY_NOFILTER =
            "SELECT * FROM %s;";
    
    /**
     * Get the next available ID
     */
    public static final String NEXT_ID_QUERY = 
            "SELECT MAX(%s) + 1 AS id FROM %s;";
    
    /**
     * New user statement to use in a PreparedStatement
     */
    public static final String NEW_USER_STATEMENT = 
            "INSERT INTO member "
            + "(id, forename, surname, password, email, height, weight, salt) VALUES "
            + "(?,?,?,?,?,?,?,?);";
    
    /**
     * Updates the whole of user to the current user data
     */
    public static final String UPDATE_USER_STATEMENT =
            "UPDATE member SET (forename, surname, email, height, weight, usergroup_name)\n" 
            + "= (?,?,?,?,?,?) WHERE id = ?;";
    
    /**
     * New goal statement to use in a PreparedStatement
     */
    public static final String NEW_GOAL_STATEMENT =
            "INSERT INTO goal "
            + "(id, goal_name, target_unit, target_date, completed, user_id, type_id, date_set) VALUES "
            + "(?,?,?,?,?,?,?,?);";
    
    /**
     * Get an exercise type using the type description
     */
    public static final String GET_EXERCISE_TYPE_ID_QUERY = 
            "SELECT id FROM exercisetype WHERE (type='%s');";
    
    /**
     * Statements for creating, and checking sessions
     */
    public static final String NEW_SESSION_STATEMENT = 
            "INSERT INTO sessions "
            + "(ip,member_id,lastseen,id) VALUES "
            + "('%s',%d,'%s','%s');";
    
    /**
     * Get the session data from a specific user and ip address
     */
    public static final String GET_SESSION_DATA_QUERY = 
            "SELECT * FROM sessions "
            + "WHERE (ip = '%s') AND (id = '%s');";
    
    /**
     * Remove all 'old' sessions using a user id
     */
    public static final String REMOVE_OLD_SESSIONS_STATEMENT = 
            "DELETE FROM sessions WHERE (member_ID = %d)";
    
    /**
     * Get the session is using a user id and an ip address
     */
    public static final String GET_SESSIONS_ID = 
            "SELECT id FROM sessions WHERE (member_ID = ?) AND (ip = ?);";
    
    /**
     * Update the last seen field on a session
     */ 
    public static final String UPDATE_SESSION_STATEMENT = 
            "UPDATE sessions SET lastseen='%s' WHERE (ip='%s') AND (id='%s');";
    
    /**
     * Delete a session from the table using the session id
     * Essentially logs out a user
     */
    public static final String LOGOUT_STATEMENT = 
            "DELETE FROM sessions WHERE (id='%s');";
    
    /**
     * New exercise statement to use in a PreparedStatement
     */
    public static final String NEW_EXERCISE_STATEMENT =
            "INSERT INTO exercise "
            + "(id, user_id, type_id, exercise_amount, date_time, total_burned) VALUES "
            + "(?,?,?,?,?,?);";
    
    /**
     * New exercise type statement to use in a PreparedStatement
     */
    public static final String NEW_EXERCISE_TYPE_STATEMENT =
            "INSERT INTO exercisetype "
            + "(id, type, unit_type, c_per_unit) VALUES "
            + "(?,?,?,?);";
    
    /**
     * New food type statement to use in a PreparedStatement
     */
    public static final String NEW_FOOD_TYPE_STATEMENT =
            "INSERT INTO foodtype "
            + "(id, food_name, calorific_count) VALUES "
            + "(?,?,?);";
    
    /**
     * New meal statement to use in a PreparedStatement
     */
    public static final String NEW_MEAL_STATEMENT = 
            "INSERT INTO meal "
            + "(id, calories, user_id, date_time) VALUES "
            + "(?,?,?,?);";
    
    /**
     * New list of meals statement to add multiple food types to a meal
     * @param numFood number of different foods in the specified meal
     * @return a suitable string to use in PreparedStatement
     */
    public static final String NEW_MEAL_LIST_STATEMENT(int numFood) {
        String output = "INSERT INTO meal_foodtype "
            + "(foodtype_id, meal_id, quantity) VALUES ";
        if(numFood > 1)
            for(int i = 0; i < numFood-1; i++)
                output += "(?,?,?), ";
        output += "(?,?,?);";
        return output;
    }
    
    /**
     * New user group statement to use in a PreparedStatement
     */
    public static final String NEW_USER_GROUP_STATEMENT = 
            "INSERT INTO usergroup "
            + "(groupname, password, groupdescription, admin) VALUES "
            + "(?,?,?,?);";
    
    /**
     * Edit user data
     */
    // <editor-fold desc="Edit user data" defaultstate="collapsed">
    public static final String EDIT_DATA_SATEMENT = 
            "UPDATE member SET %s = %s WHERE id = ?;";
    public static final String EDIT_USER_FORENAME = String.format(
            SQLConn.EDIT_DATA_SATEMENT,
            "forename", "?");
    public static final String EDIT_USER_SURNAME = String.format(
            SQLConn.EDIT_DATA_SATEMENT,
            "surname", "?");
    public static final String EDIT_USER_EMAIL = String.format(
            SQLConn.EDIT_DATA_SATEMENT,
            "email", "?");
    public static final String EDIT_USER_HEIGHT = String.format(
            SQLConn.EDIT_DATA_SATEMENT,
            "height, bmi", "?, ?");
    public static final String EDIT_USER_WEIGHT = String.format(
            SQLConn.EDIT_DATA_SATEMENT,
            "weight, bmi", "?, ?");
    public static final String EDIT_USER_PASSWORD =
            "UPDATE member SET (password, salt) VALUES (?,?) "
            + "WHERE id = ?;";
    // </editor-fold>
    
    /**
     * Delete from the meal table (non-recoverable)
     */
    public static final String DELETE_MEAL_STATEMENT =
            "DELETE FROM meal WHERE id = ?;";
    /**
     * Delete from the goal table (non-recoverable)
     */
    public static final String DELETE_GOAL_STATEMENT = 
            "DELETE FROM goal WHERE (user_id=%d) AND (goal_name='%s');";
    /**
     * Delete from the exercise table (non-recoverable)
     */
    public static final String DELETE_EXERCISE_STATEMENT = 
            "DELETE FROM exercise WHERE (id=%d);";
    
    /**
     * Kicks a user from a group
     */
    public static final String KICK_USER_GROUP_STATEMENT = 
            "UPDATE member SET usergroup_name = NULL WHERE id=?;";
    
    
    // </editor-fold>
    
    /**
     * Initialise the CONN to the SQL Server
     * @return connection Object to the SQL server
     */
    private static Connection makeConnection() {
        try {
            /**
             * Retrieve the driver class to connect using
             */
            Class.forName(JDBC_DRIVER); 
            Connection conn;
            try {
                conn = DriverManager.getConnection(SQL_IP, SQL_USER, SQL_PASS);            
                return CONN = conn;
            } catch(SQLException e) {
                System.out.println("Connecion failed");
                return null;
            }
        } catch(ClassNotFoundException e) {
            System.out.println("Class " + JDBC_DRIVER + " cannot be found");
            return null;
        }
        
    }
    
    /**
     * Get the current connection to the server, run makeConnection if there is
     * no current connection
     * @return connection to the SQL server
     */
    public static Connection getConn() {
        return CONN == null ? makeConnection() : CONN;
    }
    
    /**
     * Retrieves the next available ID from any selected table in the SQL server, using
     * the table name and the table ID's column name.
     * @param tableName
     * @param columnName
     * @return the next ID to set a record to
     */
    public static int getNextID(String tableName, String columnName) {
        String query = String.format(NEXT_ID_QUERY, columnName, tableName);
        try (Statement stm = SQLConn.getConn().createStatement()) {
            ResultSet result = stm.executeQuery(query);
            //Checking the table isn't empty
            result.next();
            return result.getInt("id");
        } catch(SQLException e) {
            System.out.println("No results");
            return 0; //Invalid ID number
        }
    }
    
    /**
     * Method to close the current connection - and as a result, any open
     * statements or other connected tasks
     */
    public static void close() {
        try {
            if(!CONN.isClosed()) {
                CONN.close();
                CONN = null;
            }
        } catch (SQLException | NullPointerException e) {
            System.out.println("Connection is already closed/doesn't exist.");
        }
    }
    
}
