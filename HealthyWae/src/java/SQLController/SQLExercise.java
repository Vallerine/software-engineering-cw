package SQLController;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.time.LocalDateTime;
import static SQLController.SQLConn.getConn;
import JavaController.Exercise;
import JavaController.User;
import JavaController.ExerciseType;

/**
 * SQL to handle SQL operations for Exercises and Exercise types
 * @author Elliot
 */
public class SQLExercise {
    
    /**
     * Static list of exercise types
     */
    public static List<ExerciseType> TYPE_LIST = getExerciseTypeList();
    
    /**
     * Insert a new type of exercise into the exerciseType table
     * @param type
     * @param unit
     * @param calsPerUnit 
     * @return if the statement completed
     */
    public static boolean newExerciseType(String type, String unit, double calsPerUnit) {
        int type_id;
        try{
            Statement stm2 = SQLConn.getConn().createStatement();
            ResultSet typeResult = stm2.executeQuery(String.format(SQLConn.GET_EXERCISE_TYPE_ID_QUERY, type));            
            typeResult.next();
            type_id = typeResult.getInt("id");
        }catch(SQLException e){
            return false;
        }
        
        
        String query = SQLConn.NEW_EXERCISE_TYPE_STATEMENT;
        try(PreparedStatement stm = SQLConn.getConn().prepareStatement(query)) {
            stm.setInt(1, SQLConn.getNextID("exercisetype", "id"));
            stm.setInt(2, type_id);
            stm.setString(3, unit);
            stm.setDouble(4, calsPerUnit);
            if(stm.executeUpdate() == 1) {
                return true;
            }
            else
                return false;
        }
        catch(SQLException e) {
            return false;
        }
    }
    
    /**
     * Comparator class to sort the exercises by date
     */
    public static class SortByDate implements Comparator<Exercise> {
        @Override
        public int compare(Exercise a, Exercise b) {
            return a.getDate().compareTo(b.getDate());
        }
    }

    /**
     * Gets an exercise using the exercise ID
     * @param id
     * @return the Exercise as an object
     */
    public static Exercise getExercise(int id){
        String query = String.format(SQLConn.GET_ALL_QUERY, "exercise", "id", id);
        try{
            Statement stm = SQLConn.getConn().createStatement();
            ResultSet result = stm.executeQuery(query);
            result.next();
            
            int type_id = result.getInt("type_id");
            int user_id = result.getInt("user_id");
            double amount = result.getFloat("exercise_amount");
            ExerciseType type = SQLExerciseType.clone(type_id);
            LocalDateTime date = result.getTimestamp("date_time").toLocalDateTime();
            User u = SQLUser.getUser(user_id);
            Exercise e = new Exercise(u, amount, type, date);
            return e;

        }catch(SQLException e){
            System.out.println("Error in SQLExercise.getExercise"+e.toString());
            return null;
        }
    }
    
    /**
     * Gets an array list of all exercises for the specific user
     * @param user
     * @return the array list 
     */
    public static ArrayList<Exercise> getExercises(User user) {
        ArrayList<Exercise> exerciseList = new ArrayList<>();
        String query = String.format(SQLConn.GET_ALL_QUERY, 
                "exercise", 
                "user_id",
                user.getUserID());
        try (Statement stm = SQLConn.getConn().createStatement()) {
            ResultSet result = stm.executeQuery(query);
            while(result.next()) {
                ExerciseType type = SQLExerciseType.clone(result.getInt("type_id"));
                double amount = result.getDouble("exercise_amount");
                LocalDateTime date = result.getTimestamp("date_time").toLocalDateTime();
                double caloriesBurned = result.getDouble("total_burned");
                exerciseList.add(new Exercise(user, amount, type, date, caloriesBurned));
            }
        }
        catch (Exception e) {
            System.out.println("Error in SQLExercise.getExercises: "+e.toString());
        }
        Collections.sort(exerciseList, new SortByDate());
        return exerciseList;
    }
    
    /**
     * Adds a new exercise to the database
     * @param exercise
     * @return whether the exercise is successfully added
     */
    public static boolean newExercise(Exercise exercise) {
        User user = exercise.getUser();
        double exerciseAmount = exercise.getExerciseAmount();
        LocalDateTime date = LocalDateTime.now();
        try {
            PreparedStatement stm = getConn().prepareStatement(SQLConn.NEW_EXERCISE_STATEMENT);
            stm.setInt(1, SQLConn.getNextID("exercise", "id"));
            stm.setInt(2, user.getUserID());
            stm.setInt(3, exercise.getType().getIndex());
            stm.setDouble(4, exerciseAmount);
            stm.setTimestamp(5, java.sql.Timestamp.valueOf(date));
            stm.setDouble(6, exercise.getType().getCalsPerUnit() * exerciseAmount);

            if(stm.executeUpdate() == 1) {
                return true;}
            else{return false;}
        }
        catch(SQLException e) {
            System.out.println("Error in SQLExercise.newExercise"+e.toString());
            return false;
        }
    }
    
    /**
     * Private method to get all exercise types in the database
     * @return exercise type list
     */
    private static List<ExerciseType> getExerciseTypeList() {
        String query = String.format(SQLConn.GET_ALL_QUERY_NOFILTER, "exercisetype");
        try(Statement stm = SQLConn.getConn().createStatement()) {
            List<ExerciseType> output = new ArrayList<>();
            ResultSet results = stm.executeQuery(query);
            while(results.next()) {
                int index = results.getInt("id");
                String type = results.getString("type");
                String unit = results.getString("unit_type");
                double calsPerUnit = results.getDouble("c_per_unit");
                output.add(new ExerciseType(index, type, unit, calsPerUnit));
            }
            return output;
        }
        catch(SQLException e) {
            return null;
        }
    }
    
    /**
     * Deletes an exercise from the table
     * @param id
     * @return whether the deletion was successful
     */
    public static boolean delete(int id){
        String query = String.format(SQLConn.DELETE_EXERCISE_STATEMENT, id);
        try{
            Statement stm = SQLConn.getConn().createStatement();
            return true;
        }catch(SQLException e){
            System.out.println("Error in SQLExercise: "+e.toString());
            return false;
        }
    }
    
    /**
     * Updates the exercise table as needed
     * @param e
     * @return whether the update was successful
     */
    public static boolean update(Exercise e){
        try{
            Statement stm = SQLConn.getConn().createStatement();
            stm.executeUpdate(String.format("UPDATE exercise SET '%s' = '%s'", "type_id", e.getType().getIndex()));
            stm.executeUpdate(String.format("UPDATE exercise SET '%s' = '%f'", "exercise_amount", e.getExerciseAmount()));
            stm.executeUpdate(String.format("UPDATE exercise SET '%s' = '%s'", "date_time", java.sql.Timestamp.valueOf(e.getDate())));
            return true;
        }catch(SQLException exc){
            System.out.println("Error in SQLExercise: "+exc.toString());
            return false;
        }
    }
    
}
