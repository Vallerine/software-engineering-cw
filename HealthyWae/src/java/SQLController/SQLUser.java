package SQLController;

import JavaController.Exercise;
import static SQLController.SQLConn.getConn;
import validations.Password;
import JavaController.User;
import JavaController.UserGroup;
import java.sql.*;
import validations.EmailValidator;

/**
 * A static class containing the SQL communication for user (member)
 * @author Elliot Kemp
 */
public class SQLUser {
    
    /**
     * Login method takes in the user's email and password directly, and returns
     * a boolean as to whether the login is successful or not
     * @param email
     * @param password
     * @return login success
     */
    public static User login(String email, String password) {
        String query = String.format(SQLConn.GET_MEMBER_QUERY, email);

        String passwordKey = null;
        String passwordCheck;

        try (Statement stm = SQLConn.getConn().createStatement()) {
            ResultSet result = stm.executeQuery(query);
            while(result.next()) {
                int userID = result.getInt("id");
                byte[] salt = Password.getArr(result.getArray("salt"));
                passwordKey = result.getString("password");
                passwordCheck = Password.getPassword(password, salt);
                if(passwordKey.equals(passwordCheck)) {
                    return new User(
                            userID,
                            email,
                            passwordKey,
                            salt,
                            result.getString("forename"),
                            result.getString("surname"),
                            result.getDouble("height"),
                            result.getDouble("weight")); 
                }else{
                    System.out.println("Password fail");
                }
            }
        }
        catch (SQLException e) {
            System.out.println("Error in SQLUser.login: "+e.toString());
        }
        //If there are no results
        if(passwordKey == null) {
            System.out.println("Login Failed");
            return null;
        } 
        return null;
    }
    
    
    

    
    
    /**
     * New user adds a new user to the database, given all the following
     * information, it generates the BMI and a hashed and salted password key,
     * along with a random salt key.
     * @param email
     * @param password
     * @param forename
     * @param surname
     * @param height
     * @param weight 
     * @return whether the addition was successful
     */
    public static boolean newUser(String email, String password, String forename,
            String surname, double height, double weight) {
        EmailValidator validator = new EmailValidator();
        if(validator.validateEmail(email)) {

            byte[] salt = Password.salt();
            String saltStr = Password.sendArr(salt);
            password = Password.getPassword(password, salt);

            try {
                PreparedStatement stm = getConn().prepareStatement(SQLConn.NEW_USER_STATEMENT);
                stm.setInt(1, SQLConn.getNextID("member", "id"));
                stm.setString(2, forename);
                stm.setString(3, surname);
                stm.setString(4, password);
                stm.setString(5, email);
                stm.setDouble(6, height);
                stm.setDouble(7, weight);
                stm.setArray(8, Password.sendObjectArr(salt));
                if(stm.executeUpdate() == 1) {
                    return true;
                }
                else
                    return false;
            } catch(SQLException e) {
                System.out.println("Exception in SQLUser.newUser" + e.toString());
                return false;
            }
        }
        else {
            System.out.println("Email is invalid!");
            return false;
        }
    }
    
    /**
     * Gets a user without it's group
     * @param member_id
     * @return the user
     */
    public static User getUserGroupless(int member_id){
        try {
            Statement stm = SQLConn.getConn().createStatement();
            ResultSet userData = stm.executeQuery(String.format(SQLConn.GET_ALL_QUERY, "member", "id", member_id));
            userData.next();
            User user = new User(member_id,
                    userData.getString("email"),
                    userData.getString("password"),
                    Password.getArr(userData.getArray("salt")),
                    userData.getString("forename"),
                    userData.getString("surname"),
                    userData.getDouble("height"),
                    userData.getDouble("weight"));
            
            String groupName = userData.getString("usergroup_name");
            if(groupName != null && !groupName.equals("")){
                Statement stm2 = SQLConn.getConn().createStatement();
                ResultSet groupData = stm2.executeQuery(String.format(SQLConn.GET_ALL_QUERY2, "usergroup", "groupname", groupName));
                groupData.next();
                groupName = groupData.getString("groupname");
                String groupDesc = groupData.getString("groupdescription");
                String password = groupData.getString("password");
                int adminId = groupData.getInt("admin");
                
                if(user.getUserID() == adminId){
                    UserGroup g = new UserGroup(groupName, password, groupDesc, user);
                    user.addGroup(g);
                }else{
                    User admin = SQLUser.getUser(adminId);
                    UserGroup g = new UserGroup(groupName, password, groupDesc, admin);
                    user.addGroup(g);
                }
            }else{
                UserGroup g = new UserGroup();
                user.addGroup(g);
            }
            user.addMeals(SQLMeal.getMeals(user));
            user.addGoals(SQLGoal.getGoals(user));
            user.addExercises(SQLExercise.getExercises(user));
            return user;}
        catch(SQLException e) {
            System.out.println("SQLUser getUser: "+e.toString());
            return null;
        }
    }
    

    /**
     * Gets a user using the member id
     * @param member_id
     * @return the user if one was found
     */
    public static User getUser(int member_id) {
        try {
            Statement stm = SQLConn.getConn().createStatement();
            ResultSet userData = stm.executeQuery(String.format(SQLConn.GET_ALL_QUERY, "member", "id", member_id));
            userData.next();
            User user = new User(member_id,
                    userData.getString("email"),
                    userData.getString("password"),
                    Password.getArr(userData.getArray("salt")),
                    userData.getString("forename"),
                    userData.getString("surname"),
                    userData.getDouble("height"),
                    userData.getDouble("weight"));
            String groupName = userData.getString("usergroup_name");
            if(groupName != null && !groupName.equals("")){
                Statement stm2 = SQLConn.getConn().createStatement();
                ResultSet groupData = stm2.executeQuery(String.format(SQLConn.GET_ALL_QUERY2, "usergroup", "groupname", groupName));
                groupData.next();
                groupName = groupData.getString("groupname");
                String groupDesc = groupData.getString("groupdescription");
                String password = groupData.getString("password");
                int adminId = groupData.getInt("admin");
                
                if(user.getUserID() == adminId){
                    UserGroup g = new UserGroup(groupName, password, groupDesc, user);
                    user.addGroup(g);
                }else{
                    User admin = SQLUser.getUser(adminId);
                    UserGroup g = UserGroup.clone(groupName);
                    
                    user.addGroup(g);
                }
            }else{
                UserGroup g = new UserGroup();
                user.addGroup(g);
            }
            user.addMeals(SQLMeal.getMeals(user));
            user.addGoals(SQLGoal.getGoals(user));
            user.addExercises(SQLExercise.getExercises(user));
            return user;}
        catch(SQLException e) {
            System.out.println("SQLUser getUser: "+e.toString());
            return null;
        }
    }
    
    /**
     * Gets a user using the user email
     * @param email
     * @return the user found (null if not)
     */
    public static User getUser(String email) {
        try {
            Statement stm = SQLConn.getConn().createStatement();
            ResultSet userData = stm.executeQuery(String.format(SQLConn.GET_ALL_QUERY2, "member", "email", email));
            userData.next();
            User user = new User(userData.getInt("id"),
                    userData.getString("email"),
                    userData.getString("password"),
                    Password.getArr(userData.getArray("salt")),
                    userData.getString("forename"),
                    userData.getString("surname"),
                    userData.getDouble("height"),
                    userData.getDouble("weight"));
            String groupName = userData.getString("usergroup_name");
            
            if(groupName != null && !groupName.equals("")){
                UserGroup g = SQLGroup.getGroup(groupName);
                user.addGroup(g);
            }else{
                UserGroup g = new UserGroup();
                user.addGroup(g);
            }
            user.addMeals(SQLMeal.getMeals(user));
            user.addGoals(SQLGoal.getGoals(user));
            return user;}
        catch(SQLException e) {
            System.out.println("SQLUser getUser: "+e.toString());
            return null;
        }
    }

    
    /**
     * Gets all user data to print as a String array
     * @param email
     * @return String array of user data
     */
    public static String[] getUserData(String email) {        
        try {
            String[] str;
            String query = String.format("SELECT * FROM member WHERE email = '%s';", email);
            try (Statement stm = SQLConn.getConn().createStatement()) {
                ResultSet result = stm.executeQuery(query);
                str = new String[7];
                while(result.next()) {
                    str[0] = "Surname: " + result.getString("surname");
                    str[1] = "Forename: " + result.getString("forename");
                    str[2] = "Email: " + result.getString("email");
                    str[3] = "Password Key: " + result.getString("password");
                    str[4] = "Salt: " + result.getString("salt");
                    str[5] = "Height: " + 
                            Double.toString(result.getDouble("height")) + "CM";
                    str[6] = "Weight: " + 
                            Double.toString(result.getDouble("weight")) + "Kg";
                }
            }
            return str;
        } catch(SQLException e) {
            System.out.println("Error in SQLUser.getUserData: "+e.toString());
            return null;
        }
    }
    
    
    /**
     * Deletes a user from the table
     * @param u
     * @return whether the deletion was successful
     */
    public static boolean delete(User u){
        try{
            Statement stm = SQLConn.getConn().createStatement();
            stm.executeUpdate(String.format("DELETE FROM TABLE member WHERE (id=%d)", u.getUserID(), u.getUserID()));
            return true;
        }catch(SQLException e){
            System.out.println("Error in SQLUser.delete: "+e.toString());
            return false;
        }  
    }
    
    
    
    /**
     * Edits a user
     * @param user
     * @param column
     * @param data
     * @return the new user once the edit has been made
     */
    public static boolean update(User user, int column, String data) {
        try {
            String query;
            switch(column) {
                case 3:
                    query = SQLConn.EDIT_USER_FORENAME;
                    break;
                case 4:
                    query = SQLConn.EDIT_USER_FORENAME;
                    break;
                case 6:
                    query = SQLConn.EDIT_USER_EMAIL;
                    break;
                case 7: //add auto bmi change later
                    query = SQLConn.EDIT_USER_HEIGHT;
                    break;
                case 8:
                    query = SQLConn.EDIT_USER_WEIGHT;
                    break;
                default:
                    return false;
            }
            PreparedStatement stm = getConn().prepareStatement(query);
            if(column == 7 || column == 8) {
                stm.setDouble(1, user.getWeight());
                stm.setDouble(2, user.bodyMassIndex());
                stm.setInt(3, user.getUserID());
            }
            else {
                stm.setString(1, data);
                stm.setInt(2, user.getUserID());
            }
            
            return stm.executeUpdate() == 1;
        }
        catch(SQLException e) {
            System.out.println("Error in SQLUser.update: " + e.toString());
            return false;
        }
    }
    
    /**
     * Update method to update the whole user
     * @param user
     * @return whether the update was successful
     */
    public static boolean update(User user) {
        String query = SQLConn.UPDATE_USER_STATEMENT;
        try { //(forename, surname, email, height, weight)
            PreparedStatement stm = SQLConn.getConn().prepareStatement(query);
            stm.setString(1, user.getForename());
            stm.setString(2, user.getSurname());
            stm.setString(3, user.getEmail());
            stm.setDouble(4, user.getHeight());
            stm.setDouble(5, user.getWeight());
            stm.setString(6, user.getGroup().getGroupName());
            stm.setInt(7, user.getUserID());
            return stm.executeUpdate() == 1;
        }
        catch (SQLException e) {
            System.out.println("Error: " + e.toString());
            return false;
        }
    }
    
    /**
     * Resets the user's password
     * @param userID
     * @param password
     * @param salt
     * @return whether the reset was successful
     */
    public static boolean resetPassword(int userID, String password, byte[] salt) {
        String query = SQLConn.EDIT_USER_PASSWORD;
        try {
            PreparedStatement stm = SQLConn.getConn().prepareStatement(query);
            stm.setInt(3, userID);
            stm.setString(1, password);
            stm.setArray(2, Password.sendObjectArr(salt));
            return stm.executeUpdate() == 1;
        }
        catch(SQLException e) {
            System.out.println("Error in SQLUser.resetPassword: " + e.toString());
            return false;
        }
    }
    
    /**
     * Gets the user in the form of a string with the user's email
     * @param email
     * @return string of the user information
     */
    public static String printUser(String email) {
        String output = "";
        for(String str : getUserData(email))
            output += str + "\n";
        return output;
    }
    
    /**
     * TESTING ONLY
     * @param args 
     */
    public static void main(String[] args) {
        //resetting password for TESTING ONLY
//        resetPassword("s.s@gmail.com", "password");
//        resetPassword("w.s@space.com", "wobble1");
//        resetPassword("jon@wibble.s", "wibble");
    }
}
