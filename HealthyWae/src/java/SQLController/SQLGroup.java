package SQLController;

import JavaController.User;
import java.sql.*;
import JavaController.UserGroup;
import java.util.ArrayList;
import java.util.List;

/**
 * SQL class to handle all user groups 
 * @author Elliot
 */
public class SQLGroup {
    
    /**
     * Updates a group
     * @param u
     * @return whether the update was successful
     */
    public static boolean updateGroup(UserGroup u){
        try{
            Statement stm = SQLConn.getConn().createStatement();
            stm.executeUpdate(String.format("UPDATE usergroup SET %s = '%s' WHERE (admin = %d)", "groupname", u.getGroupName(), u.getAdmin().getUserID()));
            stm.executeUpdate(String.format("UPDATE usergroup SET %s = '%s' WHERE (groupname = '%s')", "groupdescription", u.getDesc(), u.getGroupName()));
            stm.executeUpdate(String.format("UPDATE usergroup SET %s = '%s' WHERE (groupname = '%s')", "admin", u.getAdmin().getUserID(), u.getGroupName()));
            stm.executeUpdate(String.format("UPDATE usergroup SET %s = '%s' WHERE (groupname = '%s')", "password", u.getPassword(), u.getGroupName()));
            return true;
        }catch(SQLException e){
            System.out.println("Error in SQLGroup.updateGroup: "+e.toString());
            return false;
        }
    }


    public static List<User> getUsers(String name){
        List<User> group = new ArrayList<>();
        try{
            Statement stm2 = SQLConn.getConn().createStatement();
            ResultSet result2 = stm2.executeQuery(String.format(SQLConn.GET_ALL_QUERY2, "member", "usergroup_name", name));
            while(result2.next()){
                User memberOfGroup = SQLUser.getUserGroupless(result2.getInt("id"));
                group.add(memberOfGroup);
            }
            return group;
        }catch(SQLException e){
            System.out.println("Error in SQLGroup.getUsers"+e.toString());
            return null;
        }
        
    }

    /**
     * Gets the group with a specific group name
     * @param name
     * @return 
     */
    public static UserGroup getGroup(String name) {
        try{
            Statement stm = SQLConn.getConn().createStatement();
            ResultSet result = stm.executeQuery(String.format(SQLConn.GET_ALL_QUERY2, "usergroup", "groupname", name));
            result.next();
            
            String desc = result.getString("groupdescription");
            String pass = result.getString("password");
            int adminID = result.getInt("admin");
            
            User admin = SQLUser.getUser(adminID);
            UserGroup group = new UserGroup(name, pass, desc, admin);
            
            return group;
        }catch(SQLException e){
            System.out.println("Error in SQLGroup: "+e.toString());
            return null;
        }
    }
    
    /**
     * Deletes a user group from the table
     * @param groupName
     * @return whether the deletion was successful
     */
    public static boolean deleteGroup(String groupName){
        try{
            Statement stm = SQLConn.getConn().createStatement();
            stm.executeUpdate(String.format("DELETE FROM usergroup where (groupname='%s')", groupName));
            return true;
        }catch(SQLException e){
            System.out.println("Error at SQLGroup.deleteGroup: "+e.toString());
            return false;
        }
    }
    
    
    /**
     * Adds a new group to the database
     * @param u
     * @return whether the addition was successful
     */
    public static boolean newGroup(UserGroup u) {
        String query = SQLConn.NEW_USER_GROUP_STATEMENT;
        
        try {
            PreparedStatement stm = SQLConn.getConn().prepareStatement(query);
            stm.setString(1, u.getGroupName());
            stm.setString(2, u.getPassword());
            stm.setString(3, u.getDesc());
            stm.setInt(4, u.getAdmin().getUserID());
            return stm.executeUpdate() == 1;
        }
        catch(SQLException e) {
            System.out.println("Error in SQLGroup.newGroup: "+e.toString());
            return false;
        }
    }
    
    /**
     * Authenticates the group for the user
     * @param groupName
     * @param password
     * @return whether the authentication passed
     */
    public static boolean authenticate(String groupName, String password){
        try{
            Statement stm = SQLConn.getConn().createStatement();
            ResultSet retrievedRecord = 
                    stm.executeQuery(String.format(
                            "SELECT password FROM usergroup where (groupname='%s')", 
                            groupName));
            retrievedRecord.next();
            String retrievedPassword = retrievedRecord.getString("password");
            return retrievedPassword.equals(password);
        }catch(SQLException e){
            System.out.println("Error at SQLGroup.deleteGroup: "+e.toString());
            return false;
        }
    }
    
    /**
     * Kicks a user from a specific group
     * @param user
     * @param group
     * @return whether the operation was successful
     */
    public static boolean kickUser(User user, UserGroup group) {
        if(user.getGroup().getGroupName().equals(group.getGroupName())) {
            try {
                String query = SQLConn.KICK_USER_GROUP_STATEMENT;
                PreparedStatement stm = SQLConn.getConn().prepareStatement(query);
                stm.setInt(1, user.getUserID());
                return stm.executeUpdate() == 1;
            }
            catch (SQLException e) {
                System.out.println("Error: " + e.toString());
                return false;
            }
        }else{
            System.out.println("Error: user is not in the group!");
            return false;
        }
    }
    
}
