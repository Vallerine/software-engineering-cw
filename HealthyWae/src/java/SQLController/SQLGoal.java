package SQLController;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import static SQLController.SQLConn.getConn;
import JavaController.ExerciseGoal;
import JavaController.ExerciseType;
import JavaController.Goal;
import JavaController.User;
import java.time.LocalDate;

/**
 * A static class containing the SQL communication for user (member)
 * @author Elliot Kemp
 */
public class SQLGoal {

    /**
     * Gets an array list of all goals for the specific user
     * @param user
     * @return the array list 
     */
    public static ArrayList<Goal> getGoals(User user) {
        ArrayList<Goal> goalsList = new ArrayList<>();
        String query = String.format(SQLConn.GET_ALL_QUERY, 
                "goal", 
                "user_id",
                user.getUserID());

        try (Statement stm = SQLConn.getConn().createStatement()) {
            ResultSet result = stm.executeQuery(query);
            while(result.next()) {
                int goalID = result.getInt("id");
                String goalName = result.getString("goal_name");
                double targetFloat = result.getInt("target_unit");
                LocalDate targetDate = result.getDate("target_date").toLocalDate();
                LocalDate dateSet = result.getDate("date_set").toLocalDate();
                boolean completed = result.getBoolean("completed");
                int targetType = result.getInt("type_id");

                ExerciseType e = SQLExerciseType.clone(targetType);
                goalsList.add(new ExerciseGoal(goalID, 
                        user, goalName, targetFloat, 
                        targetDate, dateSet, completed, e));

            }
        }
        catch (Exception e) {
            System.out.println("Error in SQLGoal.getGoals: "+e.toString());
        }
        Collections.sort(goalsList);
        return goalsList;
    }
    
    
    public static boolean completeGoal(ExerciseGoal g){
        try{
            Statement stm2 = SQLConn.getConn().createStatement();
            stm2.executeUpdate(String.format("UPDATE goal SET completed=TRUE WHERE (id=%d)", g.getID()));
            return true;
        }catch(SQLException e){
            System.out.println("Error in SQLGoal.complete goal: "+e.toString());
            return false;
        }
        
    }
    
    /**
     * Adds a new goal to the database
     * @param g
     * @param u
     * @return 
     */
    public static boolean newGoal(ExerciseGoal g, User u) {
        try {
            //System.out.println(g.getType());
            Statement stm2 = SQLConn.getConn().createStatement();
            ResultSet type = stm2.executeQuery(String.format(SQLConn.GET_EXERCISE_TYPE_ID_QUERY, g.getType().getType()));            
            type.next();
            int type_id = type.getInt("id");
            PreparedStatement stm = getConn().prepareStatement(SQLConn.NEW_GOAL_STATEMENT);
            stm.setInt(1, SQLConn.getNextID("goal", "id"));
            stm.setString(2, g.getGoalName());
            stm.setDouble(3, g.getTarget());
            stm.setDate(4, java.sql.Date.valueOf(g.getTargetDate()));
            stm.setBoolean(5, false);
            stm.setInt(6, u.getUserID());
            stm.setInt(7, type_id);
            stm.setDate(8, java.sql.Date.valueOf(LocalDate.now()));
            return stm.executeUpdate() == 1;
        }
        catch(SQLException e) {
            System.out.println("Error in SQLGoal.newGoal: "+e.toString());
            return false;
        }
    }
    
    /**
     * Deletes a goal from the goals table
     * @param s
     * @param u
     * @return whether the deletion was successful
     */
    public static boolean deleteGoal(String s, User u){
        try{
            Statement stm = SQLConn.getConn().createStatement();
            stm.executeUpdate(String.format(SQLConn.DELETE_GOAL_STATEMENT, u.getUserID(), s));
            return true;
        }catch(SQLException e){
            System.out.println("Error: SQLGoal: "+e.toString());
            return false;
        }
    }
    
}
