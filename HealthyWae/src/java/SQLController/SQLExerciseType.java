package SQLController;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import JavaController.ExerciseType;


/**
 * SQL Class to handle tasks specific to exercise types
 * @author Niklas
 */
public class SQLExerciseType {
    
    /**
     * Cloning an exercise using it's name
     * @param ExerciseName
     * @return the exercise to clone
     */
    public static ExerciseType clone(String ExerciseName){
        String query = String.format(SQLConn.GET_ALL_QUERY2, "ExerciseType", "type", ExerciseName);
        try{
            Statement stm = SQLConn.getConn().createStatement();
            ResultSet result = stm.executeQuery(query);
            result.next();
            int index = result.getInt("id");
            String unitType = result.getString("unit_type");
            double c_per_unit = result.getDouble("c_per_unit");
            ExerciseType e = new ExerciseType(index, ExerciseName, unitType, c_per_unit);
            return e;
        }
        catch (SQLException e) {
            System.out.println("Error in: SQLExerciseType.clone(string): " + e.toString());
            return null;
        }
    }
    
    /**
     * Cloning an exercise using it's name
     * @param ExerciseID
     * @return 
     */
    public static ExerciseType clone(int ExerciseID){
        String query = String.format(SQLConn.GET_ALL_QUERY, "ExerciseType", "id", ExerciseID);
        try{
            Statement stm = SQLConn.getConn().createStatement();
            ResultSet result = stm.executeQuery(query);
            result.next();
            int index = result.getInt("id");
            String unitType = result.getString("unit_type");
            double c_per_unit = result.getFloat("c_per_unit");
            String ExerciseName = result.getString("type");
            ExerciseType e = new ExerciseType(index, ExerciseName, unitType, c_per_unit);
            return e;
        }
        catch (SQLException e) {
            System.out.println("Error in: SQLExerciseType.clone(int): " + e.toString());
            return null;
        }
    }
    
}
