package SQLController;

import static SQLController.SQLConn.getConn;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import JavaController.Meal;
import JavaController.FoodType;
import JavaController.User;


/**
 * A static class to handle all of the SQL needs for Meal
 * @author Elliot
 */
public class SQLMeal {
    
    /**
     * List of food types to use
     */
    public static List<FoodType> TYPE_LIST = getFoodTypeList();
    
    /**
     * Insert a new type of meal into the mealType table
     * @param foodName
     * @param calsPerUnit 
     * @return if the statement completed
     */
    public static boolean newFoodType(String foodName, double calsPerUnit) {
        String query = SQLConn.NEW_FOOD_TYPE_STATEMENT;
        try(PreparedStatement stm = SQLConn.getConn().prepareStatement(query)) {
            stm.setInt(1, SQLConn.getNextID("foodtype", "id"));
            stm.setString(2, foodName);
            stm.setDouble(3, calsPerUnit);
            if(stm.executeUpdate() == 1) {
                return true;
            }
            else
                return false;
        }
        catch(SQLException e) {
            System.out.println(e.toString() + "\nSomething went wrong. . .");
            return false;
        }
    }
    
    /**
     * Comparator class to sort the meals by date
     * Should be somewhere else?
     */
    public static class SortByDate implements Comparator<Meal> {
        @Override
        public int compare(Meal a, Meal b) {
            return a.getDate().compareTo(b.getDate());
        }
    }

    /**
     * Gets an array list of all meals for the specific user
     * @param user
     * @return the array list 
     */
    public static ArrayList<Meal> getMeals(User user) {
        ArrayList<Meal> mealList = new ArrayList<>();
        
        String query = String.format(SQLConn.GET_ALL_QUERY, "meal", "user_id", user.getUserID());
        try (Statement stm = SQLConn.getConn().createStatement()) {
            ResultSet result = stm.executeQuery(query);
            while(result.next()) {
                ArrayList<FoodType> foodTypeList = new ArrayList<>();
                ArrayList<Integer> quantityList = new ArrayList<>();

                int mealID = result.getInt("id");
                LocalDateTime date = result.getTimestamp("date_time").toLocalDateTime();
                double calories = result.getDouble("calories");
                
                String query2 = String.format(SQLConn.GET_ALL_QUERY, "meal_foodtype", "meal_id", mealID);
                try (Statement stm2 = SQLConn.getConn().createStatement()) {
                    ResultSet result2 = stm2.executeQuery(query2);
                    while(result2.next()) {
                        foodTypeList.add(TYPE_LIST.get(result2.getInt("foodtype_id")));
                        quantityList.add(result2.getInt("quantity"));
                    }
                    
                }
                FoodType[] foodTypeArray = new FoodType[foodTypeList.size()];
                foodTypeArray = foodTypeList.toArray(foodTypeArray);
                
                int[] quantityArray = new int[quantityList.size()];
                for(int i = 0; i < quantityList.size(); i++)
                    quantityArray[i] = (int)quantityList.get(i);
                
                mealList.add(new Meal(user, foodTypeArray, quantityArray, 
                        calories, date, mealID));
            }
        }
        catch (Exception e) {
            System.out.println("Error in SQLMeal.getMeals: "+e.toString());
        }
        Collections.sort(mealList, new SortByDate());
        return mealList;
    }
    
    /**
     * Adds a new meal to the database
     * @param meal
     * @return 
     */
    public static boolean newMeal(Meal meal) {
        User user = meal.getUser();
        double calories = meal.getCalories();
        LocalDateTime date = meal.getDate();
        int mealID = SQLConn.getNextID("meal", "id");
        try {
            PreparedStatement stm = 
                    getConn().prepareStatement(SQLConn.NEW_MEAL_STATEMENT);
            stm.setInt(1, mealID);
            stm.setDouble(2, calories);
            stm.setInt(3, user.getUserID());
            stm.setTimestamp(4, Timestamp.valueOf(date));
            if(stm.executeUpdate() == 1) {
                try(PreparedStatement stm2 = 
                        getConn().prepareStatement(
                                SQLConn.NEW_MEAL_LIST_STATEMENT(meal.getNumType()))) {
                    int index = 0;
                    for(int i = 1; i <= meal.getNumType()*3; i+=3) {
                        stm2.setInt(i, meal.getFoodList()[index].getTypeID());
                        stm2.setInt(i+1, mealID);
                        stm2.setInt(i+2, meal.getQuantities()[index]);
                        index++;
                    }
                    return stm2.executeUpdate() == 1;
                }
                catch(SQLException e) {
                    return false;
                }
            }
            else
                return false;
        }
        catch(SQLException e) {
            return false;
        }
    }
    
    
    /**
     * Gets a type of food using a search
     * @param key
     * @return 
     */
    public static FoodType getFoodType(String key){
        try{
            String query = String.format(SQLConn.GET_ALL_QUERY2, "foodtype", "food_name", key);
            Statement stm = SQLConn.getConn().createStatement();
            ResultSet result = stm.executeQuery(query);
            result.next();
            
            int id = result.getInt("id");
            double calories = result.getDouble("calorific_count");
            
            FoodType f = new FoodType(id, key, calories);
            return f;
        }catch(SQLException e){
            System.out.println("Error in SQLMeal.getFoodType"+e.toString());
            return null;
        }

    }

    
    /**
     * Private method to get all food types from the database
     * @return list of food types
     */
    public static List<FoodType> getFoodTypeList() {
        String query = String.format(SQLConn.GET_ALL_QUERY_NOFILTER, "foodtype");
        try(Statement stm = SQLConn.getConn().createStatement()) {
            List<FoodType> output = new ArrayList<>();
            ResultSet results = stm.executeQuery(query);
            while(results.next()) {
                int foodTypeID = results.getInt("id");
                String name = results.getString("food_name");
                double cals = results.getDouble("calorific_count");
                output.add(new FoodType(foodTypeID, name, cals));
            }
            return output;
        }
        catch(SQLException e) {
            return null;
        }
    }
    
    /**
     * Delete a meal from the meal table
     * @param mealID
     * @return whether the deletion was successful
     */
    public static boolean deleteMeal(int mealID) {
        String query = SQLConn.DELETE_MEAL_STATEMENT;
        try {
            PreparedStatement stm = SQLConn.getConn().prepareStatement(query);
            stm.setInt(1, mealID);
            return stm.executeUpdate() == 1;
        }
        catch(SQLException e) {
            System.out.println("Error: " + e.toString());
            return false;
        }
    }
}
