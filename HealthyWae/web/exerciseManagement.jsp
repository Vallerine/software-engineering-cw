<%@page import="java.time.LocalTime"%>
<%@page import="java.time.LocalDate"%>
<%@page import="java.time.ZoneId"%>
<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="JavaController.ExerciseType"%>
<%@page import="JavaController.Exercise"%>
<%@page import="SQLController.SQLExercise"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="SQLController.SQLGoal"%>
<%@page import="java.util.Date"%>
<%@page import="JavaController.ExerciseGoal"%>
<%@page import="JavaController.Goal"%>
<%@page import="JavaController.User"%>
<%@page import="JavaController.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    User user = new User(); boolean found = false; Cookie sessionID = null;  
    if(request.getCookies() != null){
        Cookie[] cookies = request.getCookies();
        for(Cookie c : cookies){
            if(c.getName().equals("sessionID")){
                found = true; sessionID = c; break;}}}
    if(found == false){ response.sendRedirect("/HealthyWae/login");
    }else{
        boolean session_success = Session.checkSession(request.getRemoteAddr(), sessionID.getValue());
        if(session_success == false){
            response.sendRedirect("/HealthyWae/login");
        }else{
            Session.updateSession(request.getRemoteAddr(), sessionID.getValue());
            user = User.getUserFromSession(request.getRemoteAddr(), sessionID.getValue());}}
    pageContext.setAttribute("user", user);
%>



<%
    user = (User) pageContext.getAttribute("user");
    if(request.getParameter("command").equals("newExercise")){
        String type = request.getParameter("type");
        ExerciseType eType = ExerciseType.clone(type);
        Double unit = Double.parseDouble(request.getParameter("unit"));
        String dateString = request.getParameter("date");
        //String timeString = request.getParameter("time");
       // String concatDateTime = dateString+":"+timeString;
        
        LocalDateTime date = LocalDateTime.from(LocalDate.parse(dateString, DateTimeFormatter.ofPattern("yyyy-MM-dd")).atStartOfDay());
      //  out.write(timeString);
      //  LocalTime timePart = LocalTime.parse(timeString, DateTimeFormatter.ofPattern("hh:mm:ss"));
        //out.write(date.getHour());
        
        
        Exercise e = new Exercise(user, unit, eType, date);
        e.submit();
        response.sendRedirect("/HealthyWae/exercise");
    }
    
    
    
    
    %>