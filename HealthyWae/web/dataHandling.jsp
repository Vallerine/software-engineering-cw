<%@page import="validations.input"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.time.LocalDate"%>
<html>
    <head></head>
    <body>
<%@ page import="JavaController.User"%>
<%@ page import="SQLController.SQLUser"%>
<%  
    if(request.getParameter("type").compareTo("register") == 0){
        out.write("<p>registering you...</p>");
        String forename = request.getParameter("forename");
        String surname = request.getParameter("surname");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String email = request.getParameter("email");
        boolean success;
        success = User.register(forename, surname, password, email);
        
        String check1 = request.getParameter("agree1");
        String check2 = request.getParameter("agree2");
        String email3 = request.getParameter("emailreq");
        
        if(!input.checkString(forename) || !input.checkString(surname)){
            response.sendRedirect("/HealthyWae/register?error="+"name must be character-only");
        }else if(!password.equals(password2)){
            response.sendRedirect("/HealthyWae/register?error="+"passwords do not match");   
        }else if(check1 == null || check2 == null || email3 == null){
            response.sendRedirect("/HealthyWae/register?error="+"please accept all agreements");   
        }else if(success){
            response.sendRedirect("/HealthyWae/");
        }else{
            response.sendRedirect("/HealthyWae/register?error="+"invalid email. are you already registered?");
        }
    }
    
    
    
    
    
    
    
    
    if(request.getParameter("type").compareTo("login") == 0){
        out.write("<p>logging you in...</p>");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String key = User.login(email, password, request.getRemoteAddr());
        
        Cookie cookie = new Cookie("sessionID", key);
        cookie.setMaxAge(60*15);
        response.addCookie(cookie);
        
        if(key != null){
            String redirectURL = "/HealthyWae/";
            response.sendRedirect(redirectURL);
        }else{
            String redirectURL = "/HealthyWae/login?error=Credentials%20Incorrect";
            response.sendRedirect(redirectURL);
        }
    }
%>
</body>
</html>