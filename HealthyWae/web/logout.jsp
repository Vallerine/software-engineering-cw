<%@page import="JavaController.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Cookie[] cookies = request.getCookies();
    Cookie sessionID = null;
    boolean found = false;
    for(Cookie c : cookies){
        if(c.getName().equals("sessionID")){
            found = true;
            sessionID = c;
            break;
        }}
    if(found == false){
        String redirectURL = "/HealthyWae/login.jsp";
        response.sendRedirect(redirectURL);
    }else{
        Session.logout(request.getRemoteAddr(), sessionID.getValue());
        sessionID.setMaxAge(-1);
        String redirectURL = "login";
        response.sendRedirect(redirectURL);
    }
%>
