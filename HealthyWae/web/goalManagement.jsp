<%@page import="java.util.regex.Matcher"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="java.time.LocalDate"%>
<%@page import="JavaController.ExerciseType"%>
<%@page import="SQLController.SQLGoal"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="JavaController.ExerciseGoal"%>
<%@page import="JavaController.Goal"%>
<%@page import="JavaController.User"%>
<%@page import="JavaController.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    User user = new User(); boolean found = false; Cookie sessionID = null;  
    if(request.getCookies() != null){
        Cookie[] cookies = request.getCookies();
        for(Cookie c : cookies){
            if(c.getName().equals("sessionID")){
                found = true; sessionID = c; break;}}}
    if(found == false){ response.sendRedirect("/HealthyWae/login.jsp");
    }else{
        boolean session_success = Session.checkSession(request.getRemoteAddr(), sessionID.getValue());
        if(session_success == false){
            response.sendRedirect("/HealthyWae/login.jsp");
        }else{
            Session.updateSession(request.getRemoteAddr(), sessionID.getValue());
            user = User.getUserFromSession(request.getRemoteAddr(), sessionID.getValue()); }}
    pageContext.setAttribute("user", user);
%>
    
    
   
<%
    user = (User) pageContext.getAttribute("user");
    
    out.write(user.toString());
    String command = request.getParameter("command");
    String redirectURL;
    
    
    //for a new goal
    if(command.equals("newGoal")){
        String goalName = request.getParameter("name");
        String type = request.getParameter("type");
        String unit = request.getParameter("unit");
        boolean success = true;
        
        
        goalName = goalName.replace("'", "\\'");
        
        try{
            Double.parseDouble(unit);
        }catch(Exception e){
            redirectURL = "/HealthyWae/goals?error=Unit%20not%20Integer";
            response.sendRedirect(redirectURL);
            success = false;
        }
        
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(goalName);
        if(m.find() == true){
        redirectURL = "/HealthyWae/goals?error=Please don't use special characters when naming goals";
            response.sendRedirect(redirectURL);
            success = false;
        }
        
        if(!success && goalName.length() > 20){
            redirectURL = "/HealthyWae/goals?error=Goal%20Name%20too%20Long";
            response.sendRedirect(redirectURL);
            success = false;
        }
        
        
        if(success == true){
            try{
                String dateString = request.getParameter("date");
                LocalDate date = LocalDate.parse(dateString, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                ExerciseType type_obj = ExerciseType.clone(type);

                Goal g = new ExerciseGoal(user, goalName, Double.parseDouble(unit), date, LocalDate.now(), false, type_obj);
                g.submit();
                redirectURL = "/HealthyWae/goals";
                response.sendRedirect(redirectURL);
            }catch(Exception e){
                out.write(e.toString());
                redirectURL = "/HealthyWae/goals?error=Unknown%20Error%20Occured";
                response.sendRedirect(redirectURL); 
            }
            
            
        }
        
        
        //for deleting goals
    }else if(command.equals("delete")){
        String goalName = request.getParameter("name");
        SQLGoal.deleteGoal(goalName, user);
        redirectURL = "/HealthyWae/goals";
        response.sendRedirect(redirectURL);
        
        
    }
    
%>