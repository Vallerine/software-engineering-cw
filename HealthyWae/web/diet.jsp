<%@page import="java.time.LocalDate"%>
<%@page import="JavaController.Meal"%>
<%@page import="JavaController.Goal"%>
<%@page import="SQLController.SQLMeal"%>
<%@page import="JavaController.FoodType"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="JavaController.User"%>
<%@page import="JavaController.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    User user = new User(); boolean found = false; Cookie sessionID = null;  
    if(request.getCookies() != null){
        Cookie[] cookies = request.getCookies();
        for(Cookie c : cookies){
            if(c.getName().equals("sessionID")){
                found = true; sessionID = c; break;}}}
    if(found == false){ response.sendRedirect("/HealthyWae/login.jsp");
    }else{
        boolean session_success = Session.checkSession(request.getRemoteAddr(), sessionID.getValue());
        if(session_success == false){
            response.sendRedirect("/HealthyWae/login.jsp");
        }else{
            Session.updateSession(request.getRemoteAddr(), sessionID.getValue());
            user = User.getUserFromSession(request.getRemoteAddr(), sessionID.getValue()); }}
    pageContext.setAttribute("user", user);
%>
<!DOCTYPE html>
<html>
<head> 
   <meta charset="UTF-8">
    <title>Diet - HealthyWae - Health, Diet &amp; Fitness Tracker</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
    <script type="text/javascript" src="JAVASCRIPT/masterScript.js"></script>
    <script type="text/javascript" src="JAVASCRIPT/dietScript.js"></script>
    <link href="CSS/stylesheet.css" rel="stylesheet" type="text/css">
</head>

<body onload="main()">
    <div class="page-wrap">
    
    <div class="header-background">
        <div class="header">
            <div class="logo">
                <h1>HealthyWae</h1> 
            </div>
            <div class="nav">
                <a href="/HealthyWae"><div class="menu">Home</div></a>
                <a href="diet"><div class="menu active">Diet</div></a>
                <a href="exercise"><div class="menu">Exercise</div></a>
                <a href="goals"><div class="menu">Goals</div></a>
                <a href="groups"><div class="menu">Groups</div></a>
                <a href="settings"><div class="menu">Account Settings</div></a>
            </div>
            <div class="login-container">
                <div class="login">
                    <button type="button" onclick="location.href='/HealthyWae/logout.jsp';">logout</button><br>
                    Welcome, 
                    <% 
                        user = (User) pageContext.getAttribute("user");
                        out.write(user.getForename()); 
                    %>

                </div>
            </div>
            <div class="icon">
                <img src=""/>
            </div>
        </div>
    </div>
        
    <div class="container title">
        <div class="row">
            <div class="col col-12">
                <h1>Diet</h1>
            </div>
        </div>
    </div>
    <div id="foodTypes" style="display: none;">
            <%
                user = (User) pageContext.getAttribute("user");
                List<FoodType> foods = SQLMeal.getFoodTypeList();
                for(FoodType f : foods){
                    out.write("<option value=\""+f.getType()+"\">"+f.getType()+"</option>");
                }
            %>             
        
    </div>
                    
    <div class="container content">
        <div class="row">
            <div class="col col-8">
                <h3>Add A New Meal</h3>
                <form action="mealManagement.jsp">
                    <input required type="hidden" name="command" value="newMeal">
                    <table class="none" id="FoodCaptureTable">
                        <tr>
                            <td></td>
                            <td>
                                <label class="failure">
                                <% out.write(request.getParameter("error")); %>          
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Date</label></td>
                            <td>
                                <input required type="date" name="name" value="<%=LocalDate.now().toString()%>">
                            </td>
                        </tr>
                        <tr>
                            <td></td><td></td>
                            <td>
                                <button type="button" onclick="newFoodCollectionRow()">Add More</button>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input class="submit" type="submit" value="Add"></td>
                        </tr>
                    </table>
                </form>
            </div>
                            
            <div class="col col-12">
            <%
                user = (User) pageContext.getAttribute("user");
                List<Meal> meals = user.getMeals();
                if(meals.size() > 0){out.write("<h3>Past Meals</h3>");}
                for(Meal m : meals){
                    out.write(String.format("<div class=\"listed\">"));
                    out.write(String.format("<span>%s</span>", m.getDate().toString()));
                    out.write(String.format("<span>%s</span>", m.getCalories()));
                    out.write(String.format("<button onclick=\"location.href='mealManagement.jsp?command=delete&id=%d'\">Delete This</button>", m.getID()));
                    out.write("</div><br>");
                }
                    %>
            </div>
        </div>
    </div>
                            
    <div class="push"></div>
    </div>
    
    <div class="footer-background">
        <div class="footer">
            HealthyWae - S.A McGuinness 100139331 | E. Kemp 100128483 | J. Stefinovic 100136806 | N. Henderson 100160371
        </div>
    </div>
</body>
</html>
