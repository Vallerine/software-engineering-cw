<%@page import="java.time.LocalDate"%>
<%@page import="JavaController.ExerciseGoal"%>
<%@page import="JavaController.Exercise"%>
<%@page import="JavaController.Goal"%>
<%@page import="JavaController.User"%>
<%@page import="SQLController.SQLUser"%>
<%@page import="JavaController.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Goals - HealthyWae - Health, Diet &amp; Fitness Tracker</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
    <link href="CSS/stylesheet.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

</head>

<body>
    <div class="page-wrap">
    <!--header stuff-->
    <div class="header-background">
        <div class="header">
            <div class="logo">
                <h1>HealthyWae</h1> 
            </div>
            <div class="nav">
                <a href="#"><div class="menu">Home</div></a>
                <a href="diet" style="visibility:hidden;"><div class="menu">Diet</div></a>
                <a href="exercise" style="visibility:hidden;"><div class="menu">Exercise</div></a>
                <a href="goals" style="visibility:hidden;"><div class="menu">Goals</div></a>
                <a href="groups" style="visibility:hidden;"><div class="menu">Groups</div></a>
                <a href="settings" style="visibility:hidden;"><div class="menu">Account Settings</div></a>
            </div>
            <div class="login-container">
                <div class="login">
                    <button type="button" onclick="location.href='/HealthyWae/logout.jsp';">&nbsp;login&nbsp;</button><br>
                </div>
            </div>
            <div class="icon">
                <img src=""/>
            </div>
        </div>
    </div>
        
    <div class="container title">
        <div class="row">
            <div class="col col-12">
                <h1>Welcome To HealthyWae</h1>
            </div>
        </div>
    </div>
        
    <div class="container content">
        <div class="row">
            <div class="col col-4">
                 <p> HealthyWae is a well-designed fitness tracker which allows you to carry out a multitude of tasks.
                    HealthyWae allows you not only to track your exercise activity, but also to set your personal exercise
                    goals, weight targets and much more. </p> <br> <br>

                    <p> <b>Worried about your diet? </b> Well HealthyWae provides the perfect solution with the Diet section. From
                    here you can save and add food types to your meals and calculate the total calorie content of each meal.
                    </p> <br> <br>

                    <p> If you are experiencing any problems with the website do not hesitate to contact our tech support team 
                    who will assist you in resolving any issues you have. </p> <br> <br>
            </div> 
            <div class="col col-6">
                    <img src="STATIC/fitness1.jpg" alt="fitness pic 1" style="width:600px;height:400px;">
            </div> 
        </div>
    </div>
    
    <div class="push"></div>
    </div>
    <div class="footer-background">
        <div class="footer">
            HealthyWae - S.A McGuinness 100139331 | E. Kemp 100128483 | J. Stefinovic 100136806 | N. Henderson 100160371
        </div>
    </div>
</body>
</html>