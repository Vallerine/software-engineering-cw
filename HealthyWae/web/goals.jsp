<%@page import="JavaController.Exercise"%>
<%@page import="JavaController.Goal"%>
<%@page import="JavaController.ExerciseType"%>
<%@page import="SQLController.SQLExercise"%>
<%@page import="java.util.List"%>
<%@page import="JavaController.User"%>
<%@page import="JavaController.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    User user = new User(); boolean found = false; Cookie sessionID = null;  
    if(request.getCookies() != null){
        Cookie[] cookies = request.getCookies();
        for(Cookie c : cookies){
            if(c.getName().equals("sessionID")){
                found = true; sessionID = c; break;}}}
    if(found == false){ response.sendRedirect("/HealthyWae/login.jsp");
    }else{
        boolean session_success = Session.checkSession(request.getRemoteAddr(), sessionID.getValue());
        if(session_success == false){
            response.sendRedirect("/HealthyWae/login.jsp");
        }else{
            Session.updateSession(request.getRemoteAddr(), sessionID.getValue());
            user = User.getUserFromSession(request.getRemoteAddr(), sessionID.getValue()); }}
    pageContext.setAttribute("user", user);
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Goals - HealthyWae - Health, Diet &amp; Fitness Tracker</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
    <link href="CSS/stylesheet.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="JAVASCRIPT/masterScript.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

</head>

<body>
    <div class="page-wrap">
    
    <div class="header-background">
        <div class="header">
            <div class="logo">
                <h1>HealthyWae</h1> 
            </div>
            <div class="nav">
                <a href="/HealthyWae"><div class="menu">Home</div></a>
                <a href="diet"><div class="menu">Diet</div></a>
                <a href="exercise"><div class="menu">Exercise</div></a>
                <a href="goals"><div class="menu active">Goals</div></a>
                <a href="groups"><div class="menu">Groups</div></a>
                <a href="settings"><div class="menu">Account Settings</div></a>
            </div>
            <div class="login-container">
                <div class="login">
                    <button type="button" onclick="location.href='/HealthyWae/logout.jsp';">Logout</button><br>
                    Welcome, 
                    <% 
                        user = (User) pageContext.getAttribute("user");
                        out.write(user.getForename()); 
                    %>
                </div>
            </div>
            <div class="icon">
                <img src=""/>
            </div>
        </div>
    </div>

    <div class="container title">
        <div class="row">
            <div class="col col-12">
                <h1>Goals</h1>
            </div>
        </div>
    </div>

    <div class="container content">
        <div class="row">
            <div class="col col-6">
                <form action="goalManagement.jsp">
                    <input required type="hidden" name="command" value="newGoal">
                    <table class="none" >
                        <tr>
                            <td></td>
                            <td>
                                <label class="failure">
                                <% out.write(request.getParameter("error")); %>          
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Goal Name</label></td>
                            <td>
                                <input required type="text" name="name">
                            </td>
                        </tr>
                        <tr>
                            <td><label>Excercise Type</label></td>
                            <td>
                                <select required name="type" id="typeSelect" onchange="setExerciseType('unitLabel', this)">

                            <%
                                List<ExerciseType> exercises = SQLExercise.TYPE_LIST;
                                for(ExerciseType e : exercises){
                                    out.write("<option value=\""+e.getType()+"\">"+e.getType()+"</option>");
                                }
                            %>                        
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Target Date</label></td>
                            <td><input required type="date" name="date"></td>
                        </tr>
                        <tr>
                            <td><label>Target Unit</label></td>
                            <td>
                                <input required type="text" name="unit" placeholder="meters" id="unitLabel">
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input class="submit" type="submit" value="Add"></td>
                        </tr>
                    </table>
                </form>
            </div>    
                        
            <div class="col col-8">
                <%
                    user = (User) pageContext.getAttribute("user");
                    List<Goal> goals = user.getGoals();
                    if(goals.size() > 0){out.write("<h3>Current Goals</h3>");}
                    double currentValue;
                    double goalTarget;
                    for(Goal g : goals){
                        currentValue = 0.0;
                        goalTarget = g.getTarget();
                        for(Exercise e : user.getExercises()){
                            if(e.getType().getType().equals(g.getType().getType()) &&
                                    e.getDate().compareTo(g.getSetDate().atStartOfDay()) >= 0 &&
                                    e.getDate().compareTo(g.getTargetDate().atStartOfDay()) <= 0){
                                currentValue += e.getExerciseAmount();
                            }
                        }
                        out.write(String.format("<div class=\"listed\">"));
                        out.write(String.format("<span>%s</span>", g.getGoalName()));
                        out.write(String.format("<span>%s</span>", g.getTargetDate().toString()));
                        if(g.getCompleted() == true){
                            out.write("<span>COMPLETED</span>");
                        }else{
                            out.write(String.format("<span>Progress: %.1f%%</span>", (currentValue/goalTarget)*100));
                        }
                        out.write(String.format("<button onclick=\"location.href='goalManagement.jsp?command=delete&name=%s'\">Delete This</button>", g.getGoalName()));
                        out.write("</div>");
                    }
                        %>
                        
            </div>
        </div>
    </div>

    <div class="container content">
    </div>

    <div class="push"></div>
    </div>
    
    <div class="footer-background">
        <div class="footer">
            HealthyWae - S.A McGuinness 100139331 | E. Kemp 100128483 | J. Stefinovic 100136806 | N. Henderson 100160371
        </div>
    </div>
</body>
</html>