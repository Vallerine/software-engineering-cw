<%@page import="SQLController.SQLUser"%>
<%@page import="validations.input"%>
<%@page import="JavaController.UserGroup"%>
<%@page import="JavaController.Session"%>
<%@page import="JavaController.User"%>
<%
    User user = new User(); boolean found = false; Cookie sessionID = null;  
    if(request.getCookies() != null){
        Cookie[] cookies = request.getCookies();
        for(Cookie c : cookies){
            if(c.getName().equals("sessionID")){
                found = true; sessionID = c; break;}}}
    if(found == false){ response.sendRedirect("/HealthyWae/login");
    }else{
        boolean session_success = Session.checkSession(request.getRemoteAddr(), sessionID.getValue());
        if(session_success == false){
            response.sendRedirect("/HealthyWae/login");
        }else{
            Session.updateSession(request.getRemoteAddr(), sessionID.getValue());
            user = User.getUserFromSession(request.getRemoteAddr(), sessionID.getValue());}}
    pageContext.setAttribute("user", user);
%>

<%
    user = (User) pageContext.getAttribute("user");
    
    String command = request.getParameter("command");
    if(command.equals("newGroup")){
        String groupName = request.getParameter("name");
        String description = request.getParameter("description");
        String password = request.getParameter("password");
        
        if(input.checkString(description) == false){
            response.sendRedirect("/HealthyWae/groups?error=no special characters allowed");
        }else{
            UserGroup group = new UserGroup(groupName, password, description, user);
            user.addGroup(group);
            group.submit();
            SQLUser.update(user);
            response.sendRedirect("/HealthyWae/groups");
        }
        
        
    }else if(command.equals("leaveGroup")){
        try{
            UserGroup u = user.getGroup();
            u.kickUser(user);
            response.sendRedirect("/HealthyWae/groups");
        }catch(Exception e){
            response.sendRedirect("/HealthyWae/groups?error="+e.toString());
        }
        
        
    }else if(command.equals("deleteGroup")){
        user.getGroup().delete();
        user.setGroup(null);
        response.sendRedirect("/HealthyWae/groups");
   
    
    }else if(command.equals("joinGroup")){
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        
        if(UserGroup.authenticate(name, password)){
            UserGroup u = UserGroup.clone(name);
            user.addGroup(u);
            user.update();
            response.sendRedirect("/HealthyWae/groups");
        }else{
            response.sendRedirect("/HealthyWae/groups?error2=password%20incorrect");
        }
        
        
    }else if(command.equals("kick")){
        try{
            int userid = Integer.parseInt(request.getParameter("id"));
            UserGroup u = user.getGroup();
            u.kickUser(SQLUser.getUser(userid));
            response.sendRedirect("/HealthyWae/groups");
        }catch(Exception e){
            response.sendRedirect("/HealthyWae/groups?error="+e.toString());
        }
    }
    
    else if(command.equals("rename")){
        out.write("<script>alert(hello!)</script>");
        String newName = request.getParameter("name");
        if(input.checkString(newName) == true){
            UserGroup g = user.getGroup();
            g.setGroupName(newName);
            g.update();
            response.sendRedirect("/HealthyWae/groups");
        }else{
            response.sendRedirect("/HealthyWae/groups?error=no characters allowed in name");
        }
    }
%>