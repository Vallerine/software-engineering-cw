<%@page import="JavaController.UserGroup"%>
<%@page import="SQLController.SQLGroup"%>
<%@page import="java.util.List"%>
<%@page import="SQLController.SQLExercise"%>
<%@page import="JavaController.ExerciseType"%>
<%@page import="JavaController.User"%>
<%@page import="JavaController.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    User user = new User(); boolean found = false; Cookie sessionID = null;  
    if(request.getCookies() != null){
        Cookie[] cookies = request.getCookies();
        for(Cookie c : cookies){
            if(c.getName().equals("sessionID")){
                found = true; sessionID = c; break;}}}
    if(found == false){ response.sendRedirect("/HealthyWae/login.jsp");
    }else{
        boolean session_success = Session.checkSession(request.getRemoteAddr(), sessionID.getValue());
        if(session_success == false){
            response.sendRedirect("/HealthyWae/login.jsp");
        }else{
            Session.updateSession(request.getRemoteAddr(), sessionID.getValue());
            user = User.getUserFromSession(request.getRemoteAddr(), sessionID.getValue()); }}
    pageContext.setAttribute("user", user);
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Excersize - HealthyWae - Health, Diet &amp; Fitness Tracker</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
    <link href="CSS/stylesheet.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div class="page-wrap">
    
    <div class="header-background">
        <div class="header">
            <div class="logo">
                <h1>HealthyWae</h1> 
            </div>
            <div class="nav">
                <a href="/HealthyWae"><div class="menu">Home</div></a>
                <a href="diet"><div class="menu">Diet</div></a>
                <a href="exercise"><div class="menu">Exercise</div></a>
                <a href="goals"><div class="menu">Goals</div></a>
                <a href="groups"><div class="menu active">Groups</div></a>
                <a href="settings"><div class="menu">Account Settings</div></a>
            </div>
            <div class="login-container">
                <div class="login">
                    <button type="button" onclick="location.href='/HealthyWae/logout.jsp';">logout</button><br>
                    Welcome,
                    <% 
                        user = (User) pageContext.getAttribute("user");
                        out.write(user.getForename()); 
                    %>
                </div>
            </div>
            <div class="icon">
                <img src=""/>
            </div>
        </div>
    </div>
        
    <div class="container title">
        <div class="row">
            <div class="col col-12">
                <h1>Groups</h1>
            </div>
        </div>
    </div>
        
    <div class="container content">
        <div class="row">
            <div class="col col-12">
                <div class="col col-4">
                    <%
                        out.write("<div class=\"listed\">");
                        user = (User) pageContext.getAttribute("user");
                        if(user.getGroup() != null && !user.getGroup().getGroupName().equals("")){
                            out.write("<span>"+user.getGroup().getGroupName()+"</span>");
                            out.write("<span>"+user.getGroup().numOfUsers()+"</span><br>");
                            out.write(""+user.getGroup().getDesc()+"<br><br>");
                            if(user.getGroup().getAdmin().getUserID() == user.getUserID()){
                                out.write(String.format("<button onclick=\"location.href='groupManagement.jsp?command=deleteGroup'\">Delete this group</button>"));
                            }else{
                                out.write(String.format("<button onclick=\"location.href='groupManagement.jsp?command=leaveGroup'\">Leave this group</button>"));
                            }
                        }
                        out.write("</div>");
                    %>
                </div>
            </div>
                
            <%
                user = (User) pageContext.getAttribute("user");
                if(user.getGroup() != null && !user.getGroup().getGroupName().equals("") && (user.getGroup().getAdmin().getUserID() == user.getUserID())){
                    out.write("<div class=\"col col-12\">");
                    out.write("<h3>Group Membership</h3>");
                    out.write("<form action=\"groupManagement.jsp\">");
                    out.write("<input type=\"hidden\" name=\"command\" value=\"rename\">");
                    out.write("Group Name: ");
                    out.write("<input type=\"text\" name=\"name\" value=\""+user.getGroup().getGroupName()+"\">");
                    out.write("&nbsp; &nbsp;<input class=\"submit\" type=\"submit\" value=\"Update\"><br>");
                    
                    UserGroup group = SQLGroup.getGroup(user.getGroup().getGroupName());
                    group.add(SQLGroup.getUsers(group.getGroupName()));
                    
                    for(User u : group.getUsers()){
                        out.write("<div class=\"listed\">");
                        out.write("<span>"+u.getForename()+"</span>");
                        if(u.getUserID() != user.getUserID()){
                            out.write(String.format("<button type=\"button\" onclick=\"location.href='groupManagement.jsp?command=kick&id=%d'\">Kick from group</button>", u.getUserID()));
                        }
                        out.write("</div><br>");
                    }
                    out.write("</form>");
                    out.write("</div>");
                }
            %>    
                
                
                
            <div class="col col-6">
                <form action="groupManagement.jsp">
                    <input type="hidden" name="command" value="newGroup">
                    Create a group:

                    <table class="none">
                        <tr>
                            <td></td>
                            <td>
                                <label class="failure">
                                <% out.write(request.getParameter("error")); %>          
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Group Name</label></td>
                            <td><input type="text" name="name"></td>
                        </tr>
                        <tr>
                            <td><label>Group Password</label></td>
                            <td><input type="password" name="password"></td>
                        </tr>
                        <tr>
                            <td><label>Group Description</label></td>
                            <td><input type="description" name="description"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input class="submit" type="submit" value="Create"></td>
                        </tr>
                    </table>

                </form>
            </div>
            
            <div class="col col-6">
                <form action="groupManagement.jsp">
                    <input type="hidden" name="command" value="joinGroup">
                    Join a group:
                    <table class="none">
                        <tr>
                            <td></td>
                            <td>
                                <label class="failure">
                                <% out.write(request.getParameter("error2")); %>          
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Group Name</label></td>
                            <td><input type="text" name="name"></td>
                        </tr>
                        <tr>
                            <td><label>Group Password</label></td>
                            <td><input type="password" name="password"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input class="submit" type="submit" value="Join"></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
        
    <div class="push"></div>
    </div>
    
    <div class="footer-background">
        <div class="footer">
            HealthyWae - S.A McGuinness 100139331 | E. Kemp 100128483 | J. Stefinovic 100136806 | N. Henderson 100160371
        </div>
    </div>
</body>
</html>