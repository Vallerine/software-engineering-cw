<%@page import="java.util.List"%>
<%@page import="JavaController.UserGroup"%>
<%@page import="java.time.temporal.ChronoUnit;"%>
<%@page import="java.util.concurrent.TimeUnit"%>
<%@page import="java.util.ArrayList"%>
<%@page import="SQLController.SQLExercise"%>
<%@page import="SQLController.SQLGroup"%>
<%@page import="java.time.LocalDate"%>
<%@page import="JavaController.ExerciseGoal"%>
<%@page import="JavaController.Exercise"%>
<%@page import="JavaController.Goal"%>
<%@page import="JavaController.User"%>
<%@page import="SQLController.SQLUser"%>
<%@page import="JavaController.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    User user = new User(); boolean found = false; Cookie sessionID = null;  
    if(request.getCookies() != null){
        Cookie[] cookies = request.getCookies();
        for(Cookie c : cookies){
            if(c.getName().equals("sessionID")){
                found = true; sessionID = c; break;}}}
    if(found == false){ response.sendRedirect("/HealthyWae/home.jsp");
    }else{
        boolean session_success = Session.checkSession(request.getRemoteAddr(), sessionID.getValue());
        if(session_success == false){
            response.sendRedirect("/HealthyWae/login.jsp");
        }else{
            Session.updateSession(request.getRemoteAddr(), sessionID.getValue());
            user = User.getUserFromSession(request.getRemoteAddr(), sessionID.getValue()); }}
    pageContext.setAttribute("user", user);
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Goals - HealthyWae - Health, Diet &amp; Fitness Tracker</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
    <link href="CSS/stylesheet.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

</head>

<body>
    <div class="page-wrap">
    <!--header stuff-->
    <div class="header-background">
        <div class="header">
            <div class="logo">
                <h1>HealthyWae</h1> 
            </div>
            <div class="nav">
                <a href="#"><div class="menu active">Home</div></a>
                <a href="diet"><div class="menu">Diet</div></a>
                <a href="exercise"><div class="menu">Exercise</div></a>
                <a href="goals"><div class="menu">Goals</div></a>
                <a href="groups"><div class="menu">Groups</div></a>
                <a href="settings"><div class="menu">Account Settings</div></a>
            </div>
            <div class="login-container">
                <div class="login">
                    <button type="button" onclick="location.href='/HealthyWae/logout.jsp';">logout</button><br>
                    Welcome,
                    <% 
                            user = (User) pageContext.getAttribute("user");
                            out.write(user.getForename()); 
                    %>
                </div>
            </div>
            <div class="icon">
                <img src=""/>
            </div>
        </div>
    </div>
        

        
    <div class="container content">
        <div class="row">
            <div class="col col-4">
                <div class="widget">
                    <h3>Welcome, <%
                        user = (User) pageContext.getAttribute("user");
                        out.write(user.getForename()+"<br>");
                        out.write(user.getWeight()+"kg<br>");
                        out.write("</h3>");
                        out.write(String.format("<span>Calories burned in the last 7 days: %.1f</span><br>", user.getScore()));
                        if(user.getGroup() != null) {out.write("Group: "+user.getGroup().getGroupName());}
                        %>
                </div>
            </div>
                
            <div class="col col-4">
                <div class="widget">
                    <h3>Update your weight</h3>
                    <form action="exerciseManagement.jsp">
                        <input type="hidden" name="command" value="newExercise">
                        <input type="hidden" name="type" value="weight">
                        <input type="date" name="date" value="<%=LocalDate.now().toString()%>">
                        <input type="number" name="unit" placeholder="new weight (kg)">
                        <input class="submit" type="submit" value="Update">
                    </form>
                </div>
            </div>
                
                
            <%
                user = (User) pageContext.getAttribute("user");
                if(user.getGroup() != null && user.getGroup().getGroupName() != ""){
                    List<User> users = SQLGroup.getUsers(user.getGroup().getGroupName());
                    //user.getGroup().add(SQLGroup.getUsers(user.getGroup().getGroupName()));
                    
                    out.write("<div class=\"col col-4\">");
                    out.write("<div class=\"widget\">");
                    out.write("<h3>"+user.getGroup().getGroupName()+"</h3><br>");
                    for(User u : users){
                        out.write(u.getForename()+" - "+u.getScore()+"<br>");
                    }
                    
                    out.write("</div>");
                    out.write("</div>");
                }
            %>  
                      
                      
                      
                    <% 
                        try{
                            user = (User) pageContext.getAttribute("user");
                            double goalTarget;
                            double currentValue;
                            for(Goal g : user.getGoals()){
                                if(g.getCompleted() == true){ continue; }
                                currentValue = 0.0;
                                goalTarget = g.getTarget();
                                for(Exercise e : user.getExercises()){
                                    if(e.getType().getType().equals(g.getType().getType()) &&
                                            e.getDate().compareTo(g.getSetDate().atStartOfDay()) >= 0 &&
                                            e.getDate().compareTo(g.getTargetDate().atStartOfDay()) <= 0){
                                        currentValue += e.getExerciseAmount();
                                    }
                                }
                                if((goalTarget-currentValue) <= 0){
                                    g.complete();
                                }
                                int daysLeft = (int) ChronoUnit.DAYS.between(LocalDate.now(), g.getTargetDate());

                                out.write("<div class=\"col col-4\">");
                                out.write("<div class=\"widget\">");
                                out.write("<h3>"+g.getGoalName()+"</h3>");
                                out.write("<span>"+g.getType().getType()+"</span><br>");
                                out.write(String.format("Progress: %.1f%%<br>", (currentValue/goalTarget)*100));
                                out.write("<progress value=\""+(currentValue/goalTarget)*100+"\" max=\"100\">"+(currentValue/goalTarget)*100+"%</progress><br>");
                                out.write(String.format("<strong>%s left</strong> %.1f k<br>", g.getType().getUnit(), (goalTarget-currentValue)));
                                out.write(String.format("<strong>Days left</strong> %d<br>", (daysLeft)));
                                out.write(String.format("<strong>%s per day required</strong> %.2f", g.getType().getUnit(), (goalTarget-currentValue)/daysLeft));
                                out.write("</div></div>");
                            }
                        }catch(Exception e){
                            e.printStackTrace();
                            System.out.println("JSP ERROR: "+e.toString());
                        }
                    %>
            
            
            
        </div>
    </div>
       
    
    
    
    
    
    
    
    
    
    
    
    
    <div class="push"></div>
    </div>
    <div class="footer-background">
        <div class="footer">
            HealthyWae - S.A McGuinness 100139331 | E. Kemp 100128483 | J. Stefinovic 100136806 | N. Henderson 100160371
        </div>
    </div>
</body>
</html>