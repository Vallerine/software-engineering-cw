<%@page import="SQLController.SQLUser"%>
<%@page import="JavaController.Session"%>
<%@page import="JavaController.User"%>
<%
    User user = new User(); boolean found = false; Cookie sessionID = null;  
    if(request.getCookies() != null){
        Cookie[] cookies = request.getCookies();
        for(Cookie c : cookies){
            if(c.getName().equals("sessionID")){
                found = true; sessionID = c; break;}}}
    if(found == false){ response.sendRedirect("/HealthyWae/login.jsp");
    }else{
        boolean session_success = Session.checkSession(request.getRemoteAddr(), sessionID.getValue());
        if(session_success == false){
            response.sendRedirect("/HealthyWae/login.jsp");
        }else{
            Session.updateSession(request.getRemoteAddr(), sessionID.getValue());
            user = User.getUserFromSession(request.getRemoteAddr(), sessionID.getValue()); }}
    pageContext.setAttribute("user", user);
%>




<%
    user = (User) pageContext.getAttribute("user");  
    if(request.getParameter("command").equals("updateDetails")){
        user.setEmail(request.getParameter("email"));
        user.setForename(request.getParameter("forename"));
        user.setSurname(request.getParameter("surname"));
        SQLUser.update(user, 1 , request.getParameter("forename"));
        response.sendRedirect("/HealthyWae/settings");
    }else if(request.getParameter("command").equals("updatePassword")){
        if(user.validatePassword(request.getParameter("old"))){
            if(request.getParameter("password").equals(request.getParameter("password2"))){
                SQLUser.resetPassword(user.getUserID(), request.getParameter("password"), user.getSalt());
                Session.logout(request.getRemoteAddr(), user.getUserID()+"");
                response.sendRedirect("/HealthyWae/login");
            }else{
                response.sendRedirect("/HealthyWae/settings?error2=please retype new password");
            }
        }else{
            response.sendRedirect("/HealthyWae/settings?error2=password incorrect");
        }
    }


%>