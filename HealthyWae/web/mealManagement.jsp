<%@page import="java.util.ArrayList"%>
<%@page import="JavaController.FoodType"%>
<%@page import="JavaController.Meal"%>
<%@page import="SQLController.SQLMeal"%>
<%@page import="JavaController.Session"%>
<%@page import="JavaController.User"%>
<%
    User user = new User(); boolean found = false; Cookie sessionID = null;  
    if(request.getCookies() != null){
        Cookie[] cookies = request.getCookies();
        for(Cookie c : cookies){
            if(c.getName().equals("sessionID")){
                found = true; sessionID = c; break;}}}
    if(found == false){ response.sendRedirect("/HealthyWae/login.jsp");
    }else{
        boolean session_success = Session.checkSession(request.getRemoteAddr(), sessionID.getValue());
        if(session_success == false){
            response.sendRedirect("/HealthyWae/login.jsp");
        }else{
            Session.updateSession(request.getRemoteAddr(), sessionID.getValue());
            user = User.getUserFromSession(request.getRemoteAddr(), sessionID.getValue()); }}
    pageContext.setAttribute("user", user);
%>


<%
    user = (User) pageContext.getAttribute("user");
    if(request.getParameter("command").equals("newMeal")){
        int tick=0;
        String type;
        String quantity;
        String error = "null";
        
        while(request.getParameter("type"+tick) != null){tick += 1;}
        
        FoodType[] foods = new FoodType[tick];
        int[] quantities = new int[tick];
        
        for(int n=0; n<tick; n++){
            try{
                type = request.getParameter("type"+n);
                FoodType fType = SQLMeal.getFoodType(type);
                quantity = request.getParameter("quantity"+n);
                foods[n] = fType;
                quantities[n] = Integer.parseInt(quantity);
            }catch(Exception e){
                error = String.format("Error with meal entry %d", n);
            }
        }
        if(error.equals("null")){
            Meal m = new Meal(user, foods, quantities, 0.0);
            SQLMeal.newMeal(m);
            response.sendRedirect("/HealthyWae/diet");
        }else{
            response.sendRedirect("/HealthyWae/diet?error="+error);
        }
        
    }else if(request.getParameter("command").equals("delete")){
        int id = Integer.parseInt(request.getParameter("id"));
        SQLMeal.deleteMeal(id);
        response.sendRedirect("/HealthyWae/diet");
        
        
    }
    


%>