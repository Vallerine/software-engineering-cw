<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>HealthyWae - Health, Diet &amp; Fitness Tracker</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
    <link href="CSS/stylesheet.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div class="page-wrap">
    
    <div class="header-background">
        <div class="header">
            <div class="logo">
                <h1>HealthyWae</h1> 
            </div>
            <div class="nav">
                <a href="/HealthyWae"><div class="menu">Home</div></a>
                <a href="diet"><div class="menu">Diet</div></a>
                <a href="exercise"><div class="menu">Exercise</div></a>
                <a href="goals"><div class="menu">Goals</div></a>
                <a href="groups"><div class="menu">Groups</div></a>
                <a href="settings"><div class="menu">Account Settings</div></a>
            </div>
            <div class="login-container">
                <div class="login">
                    
                </div>
            </div>
            <div class="icon">
                <img height="60" src=""/>
            </div>
        </div>
    </div>
        
    <div class="container title">
        <div class="row">
            <div class="col col-12">
                <h1>Log In</h1>
            </div>
        </div>
    </div>
        
    <div class="container content">
        <div class="row">
            <div class="col col-6">
                Don't have an account? <a href="register">Register here.</a>

                <form action="dataHandling.jsp" method="POST">
                    <input name="type" type="hidden" value="login">
                    <table class="none">
                        <tr>
                            <td></td>
                            <td>
                                <label class="failure">
                                <% out.write(request.getParameter("error")); %>          
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td><input type="text" placeholder="Email" name="email"></td>
                        </tr>
                        <tr>
                            <td>Password: </td>
                            <td><input type="password" placeholder="Password" name="password"></td>
                        </tr>
                    </table>
                    <input class="submit" type="submit" value="Log In"> <br>
                    <!--input class="check" type="checkbox" name="rememberme" value="Remember Me"> Remember Me<br-->
                    <a href="google.com"> Forgotten Password?</a>		<!-- change to forgotton password page -->
                <form>
            </div>
        </div>
    </div>
        
    <div class="push"></div>
    </div>
    
    <div class="footer-background">
        <div class="footer">
            HealthyWae - S.A McGuinness 100139331 | E. Kemp 100128483 | J. Stefinovic 100136806 | N. Henderson 100160371
        </div>
    </div>
</body>
</html>