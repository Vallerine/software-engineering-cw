<%@page import="java.time.LocalDate"%>
<%@page import="java.util.List"%>
<%@page import="SQLController.SQLExercise"%>
<%@page import="JavaController.ExerciseType"%>
<%@page import="JavaController.User"%>
<%@page import="JavaController.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    User user = new User(); boolean found = false; Cookie sessionID = null;  
    if(request.getCookies() != null){
        Cookie[] cookies = request.getCookies();
        for(Cookie c : cookies){
            if(c.getName().equals("sessionID")){
                found = true; sessionID = c; break;}}}
    if(found == false){ response.sendRedirect("/HealthyWae/login.jsp");
    }else{
        boolean session_success = Session.checkSession(request.getRemoteAddr(), sessionID.getValue());
        if(session_success == false){
            response.sendRedirect("/HealthyWae/login.jsp");
        }else{
            Session.updateSession(request.getRemoteAddr(), sessionID.getValue());
            user = User.getUserFromSession(request.getRemoteAddr(), sessionID.getValue()); }}
    pageContext.setAttribute("user", user);
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Excersize - HealthyWae - Health, Diet &amp; Fitness Tracker</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
    <script type="text/javascript" src="JAVASCRIPT/masterScript.js"></script>
    <link href="CSS/stylesheet.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div class="page-wrap">
    
    <div class="header-background">
        <div class="header">
            <div class="logo">
                <h1>HealthyWae</h1> 
            </div>
            <div class="nav">
                <a href="/HealthyWae"><div class="menu">Home</div></a>
                <a href="diet"><div class="menu">Diet</div></a>
                <a href="exercise"><div class="menu active">Exercise</div></a>
                <a href="goals"><div class="menu">Goals</div></a>
                <a href="groups"><div class="menu">Groups</div></a>
                <a href="settings"><div class="menu">Account Settings</div></a>
            </div>
            <div class="login-container">
                <div class="login">
                    <button type="button" onclick="location.href='/HealthyWae/logout.jsp';">logout</button><br>
                    Welcome,
                    <% 
                        user = (User) pageContext.getAttribute("user");
                        out.write(user.getForename()); 
                    %>
                </div>
            </div>
            <div class="icon">
                <img src=""/>
            </div>
        </div>
    </div>
        
    <div class="container title">
        <div class="row">
            <div class="col col-12">
                <h1>Exercise</h1>
            </div>
        </div>
    </div>
        
    <div class="container content">
        <div class="row">
            <div class="col col-8">
                <form action="exerciseManagement.jsp">
                    <input type="hidden" name="command" value="newExercise">
                    <table class="none">
                        <tr>
                            <td></td>
                            <td>
                                <label class="failure">
                                <% out.write(request.getParameter("error")); %>          
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Exercise Type</label></td>
                            <td>
                                <select name="type" name="type" onchange="setExerciseType('unitLabel', this)">
                            <%
                                List<ExerciseType> exercises = SQLExercise.TYPE_LIST;
                                for(ExerciseType e : exercises){
                                    out.write("<option value=\""+e.getType()+"\">"+e.getType()+"</option>");
                                }
                            %>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Exercise Distance/ Duration</label></td>
                            <td><input id="unitLabel" type="text" name="unit" placeholder="meters"></td>
                        </tr>
                        <tr>
                            <td><label>Date</label></td>
                            <td><input type="date" value="<%=LocalDate.now().toString()%>" name="date"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input class="submit" type="submit" value="Add"></td>
                        </tr>
                    </table>
                </form>
            </div>
                        

        </div>
    </div>
        
    <div class="push"></div>
    </div>
    
    <div class="footer-background">
        <div class="footer">
            HealthyWae - S.A McGuinness 100139331 | E. Kemp 100128483 | J. Stefinovic 100136806 | N. Henderson 100160371
        </div>
    </div>
</body>
</html>
